const ExamDate = require("../Models/ExamDate");
const HeadTableExam = require("../Models/HeadTableExam");
const { verifyData } = require('../Middleware/index');

exports.getExamDateAll = (req, res) => {
    ExamDate.findAll({
        attributes: [['Ed_id', 'edId'], ['Ed_date', 'edDate']],
        order: [['Ed_id']]
    })
        .then((data) => {
            if (data) {
                res.status(200).json(data);
            } else {
                res.status(400).json({ result: 'There is no date information in the system.' });
            }
        })
        .catch(err => {
            res.status(500).json({ Error: err.message });
        });
}

exports.getExamDateAllToSchedule = (req, res) => {
    ExamDate.findAll({
        where: {
            hte_id: req.params.hteId
        },
        attributes: [['Ed_id', 'edId'], ['Ed_date', 'edDate']],
        order: [['Ed_id']]
    })
        .then((data) => {
            if (data) {
                var date = [];
                data.forEach((resDate) => {
                    date.push(resDate.dataValues.edDate);
                })
                res.status(200).json({ date: date });
            } else {
                res.status(400).json({ result: 'There is no date information in the system.' });
            }
        })
        .catch(err => {
            res.status(500).json({ Error: err.message });
        });
}

exports.createExamDate = (req, res) => {
    const dataExamDate = {
        Ed_date: req.body.edDate,
        hte_id: req.body.hteId,
        // Ed_timeStart: req.body.edTimeStart,
        // Ed_timeEnd: req.body.edTimeEnd,
        // edDate: req.body.edDate,
        // edTimeStart: req.body.edTimeStart,
        // edTimeEnd: req.body.edTimeEnd,
    }

    ExamDate.create(dataExamDate).then(() => {
        res.status(200).json({ result: "Successfully built!" })
    })
        .catch((err) => {
            res.status(500).json({ Error: err.message });
        })
    // ExamDate.findOne({
    //     where: {
    //         Ed_date: req.body.edDate,
    //     }
    // })
    //     .then((data) => {
    //         console.log(dataExamDate)
    //         if (!data) {
    //             ExamDate.create(dataExamDate).then(() => {
    //                 res.status(200).json({ result: "Successfully built!" })
    //             })
    //         } else {
    //             res.status(400).json({ result: "Duplicate Invigilator Date information" })
    //         }
    //     })
}

exports.bulkCreateExamDate = (req, res) => {
    var dataExamDate = [];
    // Ed_date:  req.body.edDate
    const reqDate = req.body.edDate;
    reqDate.forEach((date) => {
        dataExamDate.push({
            Ed_date: date,
            hte_id: req.body.hteId
        })
    })
    ExamDate.bulkCreate(dataExamDate, { returning: true }).then(() => {
        res.status(200).json({ result: "Successfully built!" })
    })
        .catch((err) => {
            res.status(500).json({ Error: err.message });
        })
}

exports.editExamDate = (req, res) => {
    const dataExamDate = {
        Ed_date: req.body.edDate,
        // Ed_timeStart: req.body.edTimeStart,
        // Ed_timeEnd: req.body.edTimeEnd,
    }
    ExamDate.findOne({
        where: { Ed_id: req.params.edId }
    })
        .then(data => {
            if (data) {
                ExamDate.update(dataExamDate, { where: { Ed_id: req.params.edId } })
                    .then(() => {
                        res.status(200).json({ result: " Successfully Update !" });
                    })
                    .catch(err => {
                        res.status(500).json({ error: err })
                    });
            } else {
                res.status(400).json({ error: "date number does not match this date" })
            }
        })
}

exports.getExamDateById = (req, res) => {
    ExamDate.findOne({
        where: {
            Ed_id: req.params.edId,
        },
        attributes: [['Ed_id', 'edId'], ['Ed_date', 'edDate']],
        order: [['Ed_id']]
    })
        .then((data) => {
            if (data) {
                res.status(200).json(data);
            } else {
                res.status(400).json({ result: 'There is no date information in the system.' });
            }
        })
        .catch(err => {
            res.status(500).json({ Error: err.message });
        });
}

exports.deleteById = (req, res) => {
    ExamDate.destroy({
        where: {
            Ed_id: req.params.edId
        }
    })
        .then(() => {
            res.status(200).json({ result: 'Delete date Successfully !' });
        })
        .catch(err => {
            res.status(500).json({ Error: err.message });
        })
}

exports.deleteByHteId = (req, res) => {
    ExamDate.destroy({
        where: {
            hte_id: req.params.hteId
        }
    })
        .then(() => {
            res.status(200).json({ result: 'Delete date Successfully !' });
        })
        .catch(err => {
            res.status(500).json({ Error: err.message });
        })
}

exports.deleteAll = (req, res) => {
    ExamDate.destroy({
        where: {},
        truncate: true,
        restartIdentity: true
    })
        .then(() => {
            res.status(200).json({ result: 'Delete date Successfully !' });
        })
        .catch(err => {
            res.status(500).json({ Error: err.message });
        })
}

exports.getLovExamDate = (req, res) => {
    HeadTableExam.findAll({
        order: [['id', 'DESC']]
    })
        .then((hte) => {
            if (hte.length > 0) {
                ExamDate.findAll({
                    where: {
                        hte_id: hte[0].dataValues.id
                    },
                    attributes: [['Ed_id', 'code'], ['Ed_date', 'name']],
                    order: [['Ed_id']]
                })
                    .then((data) => {
                        if (data) {
                            res.status(200).json(data);
                        } else {
                            res.status(400).json({ result: 'There is no date information in the system.' });
                        }
                    })
                    .catch(err => {
                        res.status(500).json({ Error: err.message });
                    });
            } else {
                res.status(400).json({ result: 'There is no header exam table  information in the system.' })
            }
        })
}


exports.getPubExamDate = (req, res) => {
    ExamDate.findAll({
        where: {
            hte_id: req.params.hteId
        },
        attributes: [['Ed_date', 'date']],
        order: [['Ed_id']]
    })
        .then((data) => {
            if (data) {
                res.status(200).json({status: 200, result: data});
            } else {
                res.status(400).json({ result: 'There is no date information in the system.' });
            }
        })
        .catch(err => {
            res.status(500).json({ Error: err.message });
        });
}

exports.getExamDateAllByHte = (req, res) => {
    ExamDate.findAll({
        where: {
            hte_id: req.params.hteId
        },
        attributes: [['Ed_id', 'edId'], ['Ed_date', 'edDate']],
        order: [['Ed_id']]
    })
        .then((date) => {
            res.status(200).json({ status: 200, result: date });
        })
        .catch((err) => {
            res.status(500).json({ Error: err.message });
        })
}

