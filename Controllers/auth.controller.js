const config = require('../Configs/auth.config');
const User = require('../Models/User');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

exports.register = (req, res) => {
    const emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
    const userData = {
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        phone: req.body.phone,
        email: req.body.email,
        username: req.body.username,
        password: bcrypt.hashSync(req.body.password, 10),
    }

    User.findOne({
        where: {
            username: userData.username, 
            email: userData.email
        }
    })
        .then((user) => {
            if (!user) {
                if (emailRegexp.test(userData.email) && req.body.password.length >= 10) {
                    User.create(userData)
                        .then(() => {
                            res.status(200).json({
                                response: "user create Success!!"
                            })
                        })
                        .catch(err => {
                            res.status(500).json({ error: err })
                        })
                } else {
                    res.status(500).json({ error: "Email format incorrect or password incorrect " })
                }
            }else{
                res.status(500).json({ error: "user already exists"})
            }
        })
        .catch(err => {
            res.status(500).json({Error: err.message});
        })
};

exports.login = (req, res) => {
    User.findOne({
        where:{
            username : req.body.username,
        }
    })
    .then((user) => {
        if(!user){
            return res.status(404).send({ message: "User Not found."});
        }

        var passwordIsValid = bcrypt.compareSync(
            req.body.password, user.password
        );

        if(!passwordIsValid){
            return res.status(401).send({  
                accessToken: null,
                message: "Invalid Password!"
            });
        }

        var token = jwt.sign({ id: user.id, role: user.role }, config.secret, { 
            expiresIn: 86400 // 24 hours
        });

        res.status(200).json({
            id: user.id,
            role: user.role,
            accessToken: token,
        })
    })
    .catch((err) => {
        res.status(500).json({Error: err.message});
    });
};