const Subject = require('../Models/Subject');
const Period = require('../Models/Period');
const ExamDate = require("../Models/ExamDate");
const { verifyData } = require('../Middleware/index');
const { Op } = require('sequelize')
const formidable = require('formidable');
const fs = require('fs');
const path = require('path');
const XLSX = require('xlsx');

exports.createSubject = (req, res) => {
    const subjectData = {
        Sub_code: req.body.subCode,
        Sub_name: req.body.subName,
        Sub_sec: req.body.subSec,
        Sub_group_id: req.body.subGroupId,
        Sub_amount: req.body.subAmount,
        Sub_teacher: req.body.subTeacher,
        Sub_datetime: req.body.subDatetime,
        Sub_year: req.body.subYear,
        Sub_semester: req.body.subSemester,
        Per_id: req.body.perId,
        // Ed_id: req.body.edId,
    }

    Subject.findOne({
        where: {
            Sub_code: req.body.subCode,
        }
    })
        .then((sub) => {
            if (!sub) {
                Subject.create(subjectData)
                    .then(() => {
                        res.status(200).json({ result: 'Successfully Subject built !' });
                    })
                    .catch(err => {
                        res.status(500).json({ Error: err.message });
                    });
            } else {
                res.status(400).json({ result: 'Duplicate Subject information' })
            }
        })
}

exports.editSubject = (req, res) => {
    Subject.findOne({
        where: {
            Sub_id: req.params.subId,
        }
    })
        .then((sub) => {
            if (sub) {
                const subjectData = {
                    Sub_code: verifyData.checkValue(req.body.subCode, sub.Sub_code),
                    Sub_name: verifyData.checkValue(req.body.subName, sub.Sub_name),
                    Sub_sec: verifyData.checkValue(req.body.subSec, sub.Sub_sec),
                    Sub_group_id: verifyData.checkValue(req.body.subGroupId, sub.Sub_group_id),
                    Sub_amount: verifyData.checkValue(req.body.subAmount, sub.Sub_amount),
                    Sub_teacher: verifyData.checkValue(req.body.subTeacher, sub.Sub_teacher),
                    Sub_datetime: verifyData.checkValue(req.body.subDatetime, sub.Sub_datetime),
                    Sub_year: verifyData.checkValue(req.body.subYear, sub.Sub_year),
                    Sub_semester: verifyData.checkValue(req.body.subSemester, sub.Sub_semester),
                    Per_id: req.body.perId,
                }
                Subject.update(subjectData, { where: { Sub_id: req.params.subId } })
                    .then(() => {
                        res.status(200).json({ result: 'Successfully Subject Update !' });
                    })
                    .catch(err => {
                        res.status(500).json({ Error: err.message });
                    });
            } else {
                res.status(400).json({ result: 'Subject does not match this subject' })
            }
        })
}

exports.getSubjects = (req, res) => {
    Subject.findAll({
        attributes: [
            ['Sub_id', 'subId'],
            ['Sub_code', 'subCode'],
            ['Sub_name', 'subName'],
            ['Sub_sec', 'subSec'],
            ['Sub_group_id', 'subGroupId'],
            ['Sub_amount', 'subAmount'],
            ['Sub_teacher', 'subTeacher'],
            ['Sub_datetime', 'subDatetime'],
            ['Sub_year', 'subYear'],
            ['Sub_semester', 'subSemester'],
            ['Per_id', 'perId']
            // ['Ed_id', 'edId']
        ],
        order: [['Sub_id']]
    })
        .then((sub) => {
            if (sub) {
                res.status(200).json(sub);
            } else {
                res.status(400).json({ result: 'There is no subject information in the system.' });
            }
        })
        .catch(err => {
            res.status(500).json({ Error: err.message });
        });
}

exports.getSubjectsToSchedule = (req, res) => {
    Subject.belongsTo(Period, { foreignKey: 'per_id' });
    Period.hasMany(Subject, { foreignKey: 'p_id' });
    Subject.findAll({
        include: [{
            model: Period,
            required: true,
            attributes: [['p_id', 'id'], ['p_name', 'name'], ['p_startTime', 'startTime'], ['p_endTime', 'endTime']]
        }],
        attributes: [
            ['Sub_id', 'id'],
            ['Sub_code', 'subCode'],
            ['Sub_name', 'name'],
            ['Sub_sec', 'sec'],
            ['Sub_group_id', 'groupId'],
            ['Sub_amount', 'amount'],
            ['Sub_teacher', 'teacher'],
            ['Sub_datetime', 'subDatetime'],
            ['Sub_year', 'subYear'],
            ['Sub_semester', 'subSemester'],
        ],
        where: {
            // Sub_datetime: "2021-10-11"
            Sub_datetime: {
                [Op.between]: [req.body.startdate, req.body.enddate]
                // [Op.between]: ["2021-10-11", "2021-10-13"]
            }
        },
        order: [['Sub_datetime']]
    })
        .then((sub) => {
            if (sub) {
                ExamDate.findAll({
                    attributes: [['Ed_id', 'edId'], ['Ed_date', 'edDate']],
                    where: {
                        Ed_date: req.body.date
                        // Ed_date: "2021-10-12"
                    },
                    order: [['Ed_id']]
                })
                    .then((data) => {
                        if (data) {
                            var result = [];
                            let arryTitleTeacher = ["อาจารย์","ดร.", "ผู้ช่วยศาสตราจารย์", "รองศาสตราจารย์", "ศาสตราจารย์", "ผู้ช่วยศาสตราจารย์ ดร.", "ผศ. ดร.", "ผศ.", "อ."];
                            data.forEach((resDate, index) => {
                                result.push({ "date": resDate.dataValues.edDate, "subject": [] })
                                sub.forEach(resData => {
                                    if (resDate.dataValues.edDate == resData.dataValues.subDatetime) {
                                        let teacher = resData.dataValues.teacher;
                                        let teacherName = "";
                                        let teacherTitle = "";
                                        arryTitleTeacher.forEach((title) => {
                                            if(teacher.includes(title)) {
                                                teacherName = teacher.split(title)[1];
                                                teacherTitle = title;
                                                if(teacherName.split(" ").length > 2) {
                                                    if(teacherName[0] == " "){
                                                        teacherName = `${teacherName.split(" ")[1]} ${teacherName.split(" ")[2]}`
                                                    }
                                                } else {
                                                    if(teacherName[0] == " "){
                                                        teacherName = `${teacherName.split(" ")[1]}`
                                                    }
                                                }
                                            }
                                        })
                                        if (resData.period.dataValues.name == "เช้า") {
                                            result[index].subject.push(
                                                {
                                                    "id": resData.dataValues.id,
                                                    "subCode": resData.dataValues.subCode,
                                                    "name": resData.dataValues.name,
                                                    "sec": resData.dataValues.sec,
                                                    "groupId": resData.dataValues.groupId,
                                                    "amount": resData.dataValues.amount,
                                                    "teacher": resData.dataValues.teacher,
                                                    "date": resData.dataValues.subDatetime,
                                                    "subYear": resData.dataValues.subYear,
                                                    "subSemester": resData.dataValues.subSemester,
                                                    "teacherFname": teacherName.split(" ")[0],
                                                    "teacherLname": teacherName.split(" ")[1],
                                                    "teacherTitle": teacherTitle,
                                                    "time": "09:00:00"
                                                },
                                            )
                                        } else if (resData.period.dataValues.name == "บ่าย") {
                                            result[index].subject.push(
                                                {
                                                    "id": resData.dataValues.id,
                                                    "subCode": resData.dataValues.subCode,
                                                    "name": resData.dataValues.name,
                                                    "sec": resData.dataValues.sec,
                                                    "groupId": resData.dataValues.groupId,
                                                    "amount": resData.dataValues.amount,
                                                    "teacher": resData.dataValues.teacher,
                                                    "date": resData.dataValues.subDatetime,
                                                    "subYear": resData.dataValues.subYear,
                                                    "subSemester": resData.dataValues.subSemester,
                                                    "teacherFname": teacherName.split(" ")[0],
                                                    "teacherLname": teacherName.split(" ")[1],
                                                    "teacherTitle": teacherTitle,
                                                    "time": "13:00:00"
                                                },
                                            )
                                        } else if (resData.period.dataValues.name == "ค่ำ") {
                                            result[index].subject.push(
                                                {
                                                    "id": resData.dataValues.id,
                                                    "subCode": resData.dataValues.subCode,
                                                    "name": resData.dataValues.name,
                                                    "sec": resData.dataValues.sec,
                                                    "groupId": resData.dataValues.groupId,
                                                    "amount": resData.dataValues.amount,
                                                    "teacher": resData.dataValues.teacher,
                                                    "date": resData.dataValues.subDatetime,
                                                    "subYear": resData.dataValues.subYear,
                                                    "subSemester": resData.dataValues.subSemester,
                                                    "teacherFname": teacherName.split(" ")[0],
                                                    "teacherLname": teacherName.split(" ")[1],
                                                    "teacherTitle": teacherTitle,
                                                    "time": "17:00:00"
                                                },
                                            )
                                        }
                                    }
                                });
                            })
                            res.status(200).json(result);
                        } else {
                            res.status(400).json({ result: 'There is no date information in the system.' });
                        }
                    })
                    .catch(err => {
                        res.status(500).json({ Error: err.message });
                    });

                // res.status(200).json(result);
            } else {
                res.status(400).json({ result: 'There is no subject information in the system.' });
            }
        })
        .catch(err => {
            res.status(500).json({ Error: err.message });
        });
}

exports.getSubjectsById = (req, res) => {
    Subject.findOne({
        where: {
            Sub_id: req.params.subjectId
        }
    })
        .then((sub) => {
            if (sub) {
                res.status(200).json(sub);
            } else {
                res.status(404).json({ result: 'There is no subject information in the system.' });
            }
        })
        .catch(err => {
            res.status(500).json({ Error: err.message });
        });
}

exports.fileCreateSubject = (req, res) => {
    var newPath;
    const form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        if (files.file.type === null) {
            return res.status(200).json({ msg: 'No file upload' });
        }

        var oldPath = files.file.path;
        var namefile = Date.now() + '_' + files.file.name;
        newPath = path.join(__dirname, '../uploads/subjects/') + namefile;
        var rawData = fs.readFileSync(oldPath);
        var dataSubject = [];

        fs.writeFile(newPath, rawData, function (e) {
            if (e) console.log(e)
            var workbook = XLSX.readFile(newPath);
            var sheet_name_list = workbook.SheetNames;
            var xlData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);
            xlData.forEach((data) => {
                var period = "";
                if (data.period == "เช้า") {
                    period = 1;
                } else if (data.period == "บ่าย") {
                    period = 2;
                } else if (data.period == "ค่ำ") {
                    period = 3;
                }
                let newDate = parseInt(data.datetime.split("-")[2]) > 9 ? data.datetime : `${data.datetime.split("-")[0]}-${data.datetime.split("-")[1]}-0${data.datetime.split("-")[2]}`;
                const subData = {
                    Sub_code: data.code,
                    Sub_name: data.name,
                    Sub_sec: data.sec,
                    Sub_group_id: data.groupId,
                    Sub_amount: data.amount,
                    Sub_teacher: data.teacher,
                    Sub_datetime: new Date(newDate),
                    Sub_year: data.year,
                    Sub_semester: data.semester,
                    Per_id: period,
                }
                dataSubject.push(subData);
            })
            if (dataSubject.length > 1) {
                Subject.bulkCreate(dataSubject, { returning: true })
                    .then(() => {
                        fs.unlinkSync(newPath);
                        res.status(200).json({ status: 'Subject Successfully built !' });
                    })
                    .catch(_e => {
                        res.status(500).json({ error: _e })
                    })
            } else {
                res.status(400).json({ status: 400, response: 'Empty Data' });
            }
        })
    })
}

exports.deleteSubjectById = (req, res) => {
    Subject.destroy({
        where: {
            Sub_id: req.params.subId
        }
    })
        .then(() => {
            res.status(200).json({ result: 'Delete subject Successfully !' });
        })
        .catch(err => {
            res.status(500).json({ Error: err.message });
        })
}

exports.deleteSubjectAll = (req, res) => {
    try {
        Subject.findAll().then((dataSubject) => {
            if (dataSubject.length > 0) {
                Subject.destroy({
                    where: {},
                    truncate: true
                })
                    .then(() => {
                        res.status(200).json({ status: 200, response: 'Delete subject Successfully !' });
                    })
                    .catch(err => {
                        res.status(500).json({ Error: err.message });
                    })
            } else {
                res.status(200).json({ status: 200, response: 'Data Empty' });
            }
        })
    } catch (err) {
        res.status(500).json({ Error: err.message });
    }
}