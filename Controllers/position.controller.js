const Position = require("../Models/Position");

exports.getLovPosition = (req, res) => {
    Position.findAll({
        attributes: [['Pt_id', 'code'], ['Pt_name', 'name']],
        order: [['Pt_id']]
    })
        .then((position) => {
            if (position) {
                res.status(200).json(position);
            } else {
                res.status(400).json({ result: 'There is no invigilator status information in the system.' })
            }
        })
        .catch((err) => {
            res.status(500).json({Error: err.message});
        })
}