const RoomStatus = require('../Models/RoomStatus');
const { verifyData } = require('../Middleware/index');

exports.getRoomStatusAll = (req, res) => {
    RoomStatus.findAll({
        attributes: [['RS_id', 'code'], ['RS_name', 'name']],
        order: [
            ['RS_id'],
        ]
    })
        .then((resDataStatus) => {
            res.status(200).json(resDataStatus);
        })
        .catch((err) => {
            res.status(500).json({Error: err.message});
        })
}

exports.getRoomStatusById = (req, res) => {
    RoomStatus.findOne({
        where: {
            RS_id: req.params.rsId,
        },
        attributes: [['RS_id', 'code'], ['RS_name', 'name']]
    })
        .then((resDataStatus) => {
            if (resDataStatus) {
                res.status(200).json(resDataStatus);
            } else {
                res.status(400).json({ result: 'There is no status information in the system.' });
            }
        })
        .catch((err) => {
            res.status(500).json({Error: err.message});
        })
}

exports.createRoomStatus = (req, res) => {
    if (req.body.rsName) {
        RoomStatus.findOne({ where: { RS_name: req.body.rsName } })
            .then((roomStatus) => {
                if (!roomStatus) {
                    RoomStatus.create({ RS_name: req.body.rsName })
                        .then(() => {
                            res.status(200).json({ result: 'Successfully built !' });
                        })
                        .catch(err => {
                            res.status(500).json({Error: err.message});
                        });
                } else {
                    res.status(400).json({ result: 'Duplicate room Status information' });
                }
            })
    } else {
        res.status(400).json({ result: 'This is status name null.' });
    }
}

exports.deleteRoomStatusById = (req, res) => {
    RoomStatus.destroy({
        where: {
            RS_id: req.params.rsId
        }
    })
        .then(() => {
            res.status(200).json({ result: 'Delete room Status Successfully !' });
        })
        .catch(err => {
            res.status(500).json({Error: err.message});
        })
}

exports.deleteRoomStatusAll = (req, res) => {
    RoomStatus.destroy({
        where: {},
        truncate: true
    })
        .then(() => {
            res.status(200).json({ result: 'Delete room Status Successfully !' });
        })
        .catch(err => {
            res.status(500).json({Error: err.message});
        })
}

exports.editRoomStatus = (req, res) => {
    RoomStatus.findOne({
        where: {RS_id: req.params.rsId },
    })
    .then((roomStatus) => {
        if(roomStatus){
            RoomStatus.update({RS_name: verifyData.checkValue(req.body.rsName, roomStatus.RS_name)  }, {where:{RS_id: req.params.rsId}})
            .then(() => {
                res.status(200).json({ result: 'Successfully update !' });
            })
            .catch(err => {
                res.status(500).json({Error: err.message});
            });
        }else {
            res.status(400).json({ result: 'There is no room status information in the system.' })
        }
    })
}