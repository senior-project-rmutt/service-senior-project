const Invigilator = require("../Models/Invigilator");
const InvigilatorStatus = require("../Models/InvigilatorStatus");
const Position = require("../Models/Position");
const { fn, col } = Invigilator.sequelize;

const formidable = require('formidable');
const fs = require('fs');
const path = require('path');
const XLSX = require('xlsx');

function spillStrings(data) {
    return data.split(" ");
}

exports.getInv = (req, res) => {
    Invigilator.belongsTo(InvigilatorStatus, { foreignKey: 'IS_id' })
    InvigilatorStatus.hasMany(Invigilator, { foreignKey: 'IS_id' })
    Invigilator.belongsTo(Position, { foreignKey: 'Pt_id' })
    Position.hasMany(Invigilator, { foreignKey: 'Pt_id' })

    Invigilator.findAll({
        include: [
            {
                model: InvigilatorStatus,
                required: true,
                attributes: [['IS_id', 'id'], ['IS_name', 'name']]
            },
            {
                model: Position,
                required: true,
                attributes: [['Pt_id', 'id'], ['Pt_name', 'name']]
            },
        ],
        attributes: [['Inv_id', 'invId'], ['Inv_prefix', 'invPrefix'], ['Inv_firstname', 'invFirstname'], ['Inv_lastname', 'invLastname'], ['Inv_phone', 'invPhone'], ['Inv_lineid', 'invLineid']],
        order: [
            ['Inv_id'],
        ]
    })
        .then(inv => {
            res.status(200).json(inv)
        })
        .catch((err) => {
            res.status(500).json({ Error: err.message });
        })
}

exports.getInvToschedule = (req, res) => {
    Invigilator.belongsTo(InvigilatorStatus, { foreignKey: 'IS_id' })
    InvigilatorStatus.hasMany(Invigilator, { foreignKey: 'IS_id' })
    Invigilator.belongsTo(Position, { foreignKey: 'Pt_id' })
    Position.hasMany(Invigilator, { foreignKey: 'Pt_id' })

    Invigilator.findAll({
        include: [
            {
                model: InvigilatorStatus,
                required: true,
                attributes: [['IS_id', 'id'], ['IS_name', 'name']]
            },
            {
                model: Position,
                required: true,
                attributes: [['Pt_id', 'id'], ['Pt_name', 'name']]
            },
        ],
        where: {
            IS_id: 1
        },
        attributes: [['Inv_id', 'invId'], ['Inv_prefix', 'invPrefix'], ['Inv_firstname', 'invFirstname'], ['Inv_lastname', 'invLastname'], ['Inv_phone', 'invPhone'], ['Inv_lineid', 'invLineid']],
        order: [
            ['Inv_id'],
        ]
    })
        .then(inv => {
            res.status(200).json({ result: inv })
        })
        .catch((err) => {
            res.status(500).json({ Error: err.message });
        })
}

exports.getInvById = (req, res) => {
    Invigilator.belongsTo(InvigilatorStatus, { foreignKey: 'IS_id' })
    InvigilatorStatus.hasMany(Invigilator, { foreignKey: 'IS_id' })
    Invigilator.findOne({
        where: {
            Inv_id: req.params.invId
        },
        include: [{
            model: InvigilatorStatus,
            required: true,
            attributes: [['IS_id', 'id'], ['IS_name', 'name']]
        }],
        attributes: [['Inv_id', 'invId'], ['Inv_prefix', 'invPrefix'], ['Inv_firstname', 'invFirstname'], ['Inv_lastname', 'invLastname'], ['Inv_phone', 'invPhone'], ['Inv_lineid', 'invLineid']],
        order: [
            ['Inv_id'],
        ]
    })
        .then(inv => {
            if (inv) {
                res.status(200).json(inv);
            } else {
                res.status(400).json({ result: 'There is no invigilator information in the system.' });
            }
        })
        .catch((err) => {
            res.status(500).json({ Error: err.message });
        })
}

exports.createInv = (req, res) => {
    const invData = {
        Inv_prefix: req.body.invPrefix,
        Inv_firstname: req.body.invFirstname,
        Inv_lastname: req.body.invLastname,
        Inv_phone: req.body.invPhone,
        Inv_lineid: req.body.invLineid,
        IS_id: req.body.isId,
        Pt_id: req.body.ptId,
    }

    Invigilator.create(invData)
        .then(() => {
            res.status(200).json({ result: ' Successfully !' })
        })
        .catch(err => {
            res.status(500).json({ Error: err.message });
        })
}

exports.editInv = (req, res) => {
    const invData = {
        Inv_prefix: req.body.invPrefix,
        Inv_firstname: req.body.invFirstname,
        Inv_lastname: req.body.invLastname,
        Inv_phone: req.body.invPhone,
        Inv_lineid: req.body.invLineid,
        IS_id: req.body.isId,
        Pt_id: req.body.ptId,
    }

    Invigilator.findOne({
        where: { Inv_id: req.params.invId }
    })
        .then(data => {
            if (data) {
                Invigilator.update(invData, { where: { Inv_id: req.params.invId } })
                    .then((inv) => {
                        res.status(200).json({ status: ' Successfully Update !' });
                    })
            } else {
                res.json({ error: "Invigilator does not match this Invigilator" })
            }
        })
}

exports.fileCreateInvigilator = (req, res) => {
    var newPath;
    const form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        if (files.file.type === null) {
            return res.status(200).json({ msg: 'No file upload' });
        }

        var oldPath = files.file.path;
        var namefile = Date.now() + '_' + files.file.name;
        newPath = path.join(__dirname, '../uploads/invs/')
            + namefile
        var rawData = fs.readFileSync(oldPath)

        fs.writeFile(newPath, rawData, function (Err) {
            if (Err) console.log(Err)
            var workbook = XLSX.readFile(path.join(__dirname, '../uploads/invs/' + namefile));
            var sheet_name_list = workbook.SheetNames;
            var xlData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);

            for (let data of xlData) {
                var status = data.status;
                if (data.status == "ปกติ") {
                    status = 1;
                } else if (data.status == "ไม่ว่าง") {
                    status = 2;
                } else if (data.status == "เกษียณอายุ") {
                    status = 3;
                } else if (data.status == "ลาป่วย") {
                    status = 4;
                } else if (data.status == "ลากิจ") {
                    status = 5;
                }
                var position = data.position;
                if (data.position == "เจ้าหน้าที่") {
                    position = 1;
                } else if (data.position == "อาจารย์") {
                    position = 2;
                } else if (data.position == "หัวหน้างาน") {
                    position = 3;
                } else if (data.position == "คณบดี") {
                    position = 4;
                } else if (data.position == "รองคณบดี") {
                    position = 5;
                } else if (data.position == "หัวหน้าสาขา") {
                    position = 6;
                } else if (data.position == "ผู้ช่วยอธิการบดี") {
                    position = 7;
                }

                const fullname = spillStrings(data.fullname);
                const invsData = {
                    Inv_prefix: data.prefix,
                    Inv_firstname: fullname[0],
                    Inv_lastname: fullname[1],
                    Inv_phone: data.phone,
                    Inv_lineid: data.lineid,
                    IS_id: status,
                    Pt_id: position,
                }
                Invigilator.findOne({
                    where: {
                        Inv_firstname: fullname[0],
                        Inv_lastname: fullname[1]
                    }
                })
                    .then(invs => {
                        if (invs) {
                            Invigilator.update(invsData, { where: { Inv_firstname: fullname[0], Inv_lastname: fullname[1] } })
                        } else {
                            Invigilator.create(invsData)
                        }
                    })
                    .catch(e => {
                        res.status(500).json({ Error: e.message });
                    })
            }
            fs.unlinkSync(newPath);
            res.status(200).json({ status: 'Invigilators Update and Successfully built !' })
        })
    })
}

exports.getLovInv = (req, res) => {
    Invigilator.belongsTo(InvigilatorStatus, { foreignKey: 'IS_id' })
    InvigilatorStatus.hasMany(Invigilator, { foreignKey: 'IS_id' })

    Invigilator.findAll({
        include: [
            {
                model: InvigilatorStatus,
                required: true,
                attributes: [['IS_id', 'id'], ['IS_name', 'name']]
            }
        ],
        where: {
            IS_id: 1
        },
        attributes: [['Inv_id', 'code'], [fn('concat', col('Inv_firstname'), ' ', col('Inv_lastname')), "name"]],
        order: [
            ['Inv_id'],
        ]
    })
        .then(inv => {
            res.status(200).json(inv)
        })
        .catch((err) => {
            res.status(500).json({ Error: err.message });
        })
}

exports.deleteInvigilators = (req, res) => {
    Invigilator.findAll().then((dataInv) => {
        if (dataInv.length > 0) {
            Invigilator.destroy({
                where: {},
                truncate: true,
            })
                .then(() => {
                    res.status(200).json({ status: 200, response: "Delete Successfuly" })
                })
                .catch((err) => {
                    res.status(500).json({ Error: err.message });
                })
        } else {
            res.status(200).json({ status: 200, response: "Data Empty" })
        }
    })
}