// const pdfTemplate = require('../documents');
const pdf = require("html-pdf");
const ejs = require("ejs");
let path = require("path");
const fs = require("fs");
const { PDFDocument } = require("pdf-lib");

exports.createExamPDF = (req, res) => {
    try {
        const newData = req.body.data;
        let newPath = path.join(__dirname, "../Other/pdf/")
        var arrName = [];
        for (let p = 0, dLen = newData.length; p < dLen; p++) {
            let newDate = `${newData[p].date.split('-')[2]}/${newData[p].date.split('-')[1]}/${newData[p].date.split('-')[0]}`;
            newData[p].subjects.forEach((subject) => {
                ejs.renderFile(
                    path.join(
                        __dirname,
                        "../documents/",
                        "report-template-tabe-exam-new.ejs"
                    ),
                    { tableExam: subject, date: newDate, semester: newData[p].semester, tableName: newData[p].tableName, year: newData[p].year },
                    (_err, data) => {
                        if (_err) {
                            res.status(500).json(_err.message)
                        } else {
                            // dataHtml.push(data)
                            let newFilename = "report-" + Date.now();
                            let options = {
                                height: "10.26771654in",
                                width: "15in",
                                orientation: "portrait",
                                type: "pdf",
                                renderDelay: 1000,
                            };
                            pdf.create(data, options).toFile(
                                newPath + newFilename + ".pdf",
                                function (err, _data) {
                                    if (err) {
                                        res.status(500).send(err.message);
                                    } else {
                                        arrName.push(newFilename + ".pdf");
                                        if (arrName.length == req.body.count) {
                                            mergePDFArray(arrName.sort()).then((d) => {
                                                for (const filename of arrName) {
                                                    fs.unlinkSync(newPath + filename);
                                                }
                                                return res.status(200).json({ filename: d });
                                            });
                                        }
                                    }
                                }
                            );
                        }
                    }
                );
            })
        }
    } catch (err) {
        res.status(500).send(error.message);
    }
};

exports.createExamPDFV2 = (req, res) => {
    try {
        // const newData = req.body.data;
        var arrBuffer = [];
        var arrHTML = []
        Promise.all(req.body.data).then((newData) => {
            for (let p = 0, dLen = newData.length; p < dLen; p++) {
                let newDate = `${newData[p].date.split('-')[2]}/${newData[p].date.split('-')[1]}/${newData[p].date.split('-')[0]}`;
                newData[p].subjects.forEach((subject) => {
                    ejs.renderFile(
                        path.join(
                            __dirname,
                            "../documents/",
                            "report-template-tabe-exam-new.ejs"
                        ),
                        { tableExam: subject, date: newDate, semester: newData[p].semester, tableName: newData[p].tableName, year: newData[p].year },
                        (_err, data) => {
                            if (_err) {
                                res.status(500).json(_err.message)
                            } else {
                                arrHTML.push(data)
                            }
                        }
                    );
                })
            }
            Promise.all(arrHTML).then((dataHTML) => {
                dataHTML.forEach((data) => {
                    let options = {
                        height: "10.26771654in",
                        width: "15in",
                        orientation: "portrait",
                        type: "pdf",
                        // renderDelay: 100000,
                        timeout: 150000,
                    };
                    pdf.create(data, options).toBuffer((err, dataBuffer) => {
                        if (err) {
                            return console.error(err)
                        } else {
                            if (!!dataBuffer?.buffer) {
                                arrBuffer.push(dataBuffer)
                                if (arrBuffer.length == req.body.count) {
                                    mergePDFArrayV2(arrBuffer).then((d) => {
                                        return res.status(200).json({ filename: d });
                                    });
                                }
                            }
                        }
                    })
                })
            })
        });
    } catch (err) {
        res.status(500).send(error.message);
    }
};

exports.getExamPDF = (req, res) => {
    try {
        let newPath = path.join(__dirname, "../Other/pdf/") + req.params.id + ".pdf";
        res.sendFile(newPath);
    } catch (error) {
        res.status(500).send(error.message);
    }
};

exports.getImageLogo = (req, res) => {
    res.sendFile(
        path.resolve(path.resolve(__dirname, "../documents/img/logo.png"))
    );
};

exports.deletePDF = (req, res) => {
    try {
        fs.unlinkSync(path.join(__dirname, "../Other/pdf/" + req.params.filename + ".pdf"));
        return res.status(200).json({ status: 200, statusName: "Successfully" })
    } catch (err) {
        return res.status(500).json({ status: 500, statusName: err.message });
    }
};

async function mergePDF(file1, file2) {
    const pathFile = path.join(__dirname, "../Other/pdf/");
    const cover = await PDFDocument.load(fs.readFileSync(pathFile + file1));
    const content = await PDFDocument.load(fs.readFileSync(pathFile + file2));

    const doc = await PDFDocument.create();

    const contentPages1 = await doc.copyPages(cover, cover.getPageIndices());
    for (const page of contentPages1) {
        doc.addPage(page);
    }

    const contentPages2 = await doc.copyPages(content, content.getPageIndices());
    for (const page of contentPages2) {
        doc.addPage(page);
    }

    fs.writeFileSync(pathFile + "NewTestMerge.pdf", await doc.save());
}

async function mergePDFArray(fileName) {
    const pathFile = path.join(__dirname, "../Other/pdf/");
    const doc = await PDFDocument.create();
    var contentPages = [];
    for (const file of fileName) {
        const cover = await PDFDocument.load(fs.readFileSync(pathFile + file));
        contentPages.push(await doc.copyPages(cover, cover.getPageIndices()));
    }
    return Promise.all(contentPages).then(async (content) => {
        var newFileName = `report-${Date.now()}`;
        content.forEach((page) => {
            for (const p of page) {
                doc.addPage(p);
            }
        })
        fs.writeFileSync(pathFile + `${newFileName}.pdf`, await doc.save());
        return newFileName;
    })
}

async function mergePDFArrayV2(dataBuffer) {
    const pathFile = path.join(__dirname, "../Other/pdf/");
    const doc = await PDFDocument.create();
    var contentPages = [];
    for (const buffer of dataBuffer) {
        const cover = await PDFDocument.load(buffer);
        contentPages.push(await doc.copyPages(cover, cover.getPageIndices()));
    }
    return Promise.all(contentPages).then(async (content) => {
        var newFileName = `report-${Date.now()}`;
        content.forEach((page) => {
            for (const p of page) {
                doc.addPage(p);
            }
        })
        fs.writeFileSync(pathFile + `${newFileName}.pdf`, await doc.save());
        return newFileName;
    })
}

async function createPDF(value) {
    const newData = value;
    let newPath = path.join(__dirname, "../Other/pdf/")
    var arrName = [];
    for (const v of newData) {
        let newDate = `${v.date.split('-')[2]}/${v.date.split('-')[1]}/${v.date.split('-')[0]}`;
        for (const sub of v.subjects) {
            let newFilename = "report-" + Date.now();
            ejs.renderFile(
                path.join(
                    __dirname,
                    "../documents/",
                    "report-template-tabe-exam-new.ejs"
                ),
                { tableExam: sub, date: newDate, semester: v.semester, tableName: v.tableName, year: v.year },
                (_err, data) => {
                    if (_err) {
                        console.log(_err);
                    } else {
                        // "height": "11.25in",
                        //      "width": "8.5in",
                        let options = {
                            // "height": "8.26771654in",
                            height: "10.26771654in",
                            width: "15in",
                            // "width": "11.9in",
                            //   "height": "8.26771654in",
                            //   "width": "11.6929134in",
                            orientation: "portrait",
                            //   "header": {
                            //       "height": "20mm",
                            //   },
                            //   "footer": {
                            //       "height": "20mm",
                            //   },
                        };
                        pdf.create(data, options).toFile(
                            newPath + newFilename + ".pdf",
                            function (err, _data) {
                                if (err) {
                                    res.send(err);
                                } else {
                                    arrName.push(newFilename + ".pdf");
                                    if (arrName.length == (newData.length * 3)) {
                                        mergePDFArray(arrName.sort()).then((d) => {
                                            for (const filename of arrName) {
                                                fs.unlinkSync(newPath + filename);
                                            }
                                            return d;
                                        });
                                    }
                                }
                            }
                        );
                    }
                }
            );
        }
    }
}
