const ExamTable = require('../Models/ExamTable');
const Sequelize = require('sequelize');
const { Op } = require('sequelize')

exports.getTableExamByHte = (req, res) => {
    ExamTable.findAll({
        where: {
            hte_id: req.params.hteId
        },
        order: [['id']]
    })
        .then((data) => {
            if (data) {
                res.status(200).json(data);
            } else {
                res.status(400).json({ result: 'There is no exam table  information in the system.' })
            }
        })
}

exports.getTableExam = (req, res) => {
    ExamTable.findAll({
        order: [['id']]
    })
        .then((data) => {
            if (data) {
                res.status(200).json(data);
            } else {
                res.status(400).json({ result: 'There is no exam table  information in the system.' })
            }
        })
}

exports.getTableExamByCondition = (req, res) => {
    let whereDynamic1 = {};
    let whereDynamic2 = {};
    if (req.body.codeSub == null || req.body.codeSub == "") {
        whereDynamic1.code_sub = { [Op.not]: null };
        whereDynamic2.code_sub = { [Op.not]: null };
    } else {
        whereDynamic1.code_sub = { [Op.substring]: req.body.codeSub };
        whereDynamic2.code_sub = { [Op.substring]: req.body.codeSub };
    }
    if (req.body.semester == null || req.body.semester == "") {
        whereDynamic1.semester = { [Op.not]: null };
        whereDynamic2.semester = { [Op.not]: null };
    } else {
        whereDynamic1.semester = { [Op.substring]: req.body.semester };
        whereDynamic2.semester = { [Op.substring]: req.body.semester };
    }
    if (req.body.year == null || req.body.year == "") {
        whereDynamic1.year = { [Op.not]: null };
        whereDynamic2.year = { [Op.not]: null };
    } else {
        whereDynamic1.year = { [Op.substring]: req.body.year };
        whereDynamic2.year = { [Op.substring]: req.body.year };
    }
    if (req.body.subName == null || req.body.subName == "") {
        whereDynamic1.sub_name = { [Op.not]: null };
        whereDynamic2.sub_name = { [Op.not]: null };
    } else {
        whereDynamic1.sub_name = { [Op.substring]: req.body.subName };
        whereDynamic2.sub_name = { [Op.substring]: req.body.subName };
    }
    if (req.body.groupId == null || req.body.groupId == "") {
        whereDynamic1.group_id = { [Op.not]: null };
        whereDynamic2.group_id = { [Op.not]: null };
    } else {
        whereDynamic1.group_id = { [Op.substring]: req.body.groupId };
        whereDynamic2.group_id = { [Op.substring]: req.body.groupId };
    }
    if (req.body.date == null || req.body.date == "") {
        whereDynamic1.date = { [Op.not]: null };
        whereDynamic2.date = { [Op.not]: null };
    } else {
        whereDynamic1.date = { [Op.between]: [req.body.date, req.body.endDate] };
        whereDynamic2.date = { [Op.between]: [req.body.date, req.body.endDate] };
    }
    if (req.body.teacher == null || req.body.teacher == "") {
        whereDynamic1.teacher = { [Op.not]: null };
        whereDynamic2.teacher = { [Op.not]: null };
    } else {
        whereDynamic1.teacher = { [Op.substring]: req.body.teacher };
        whereDynamic2.teacher = { [Op.substring]: req.body.teacher };
    }
    if (req.body.roomName == null || req.body.roomName == "") {
        whereDynamic1.room_name = { [Op.not]: null };
        whereDynamic2.room_name = { [Op.not]: null };
    } else {
        whereDynamic1.room_name = { [Op.substring]: req.body.roomName };
        whereDynamic2.room_name = { [Op.substring]: req.body.roomName };
    }
    if (req.body.inv == null || req.body.inv == "") {
        whereDynamic1.inv1 = { [Op.not]: null };
        whereDynamic2.inv2 = { [Op.not]: null };
    } else {
        whereDynamic1.inv1 = { [Op.substring]: req.body.inv };
        whereDynamic2.inv2 = { [Op.substring]: req.body.inv };
    }
    if (req.body.pId == null || req.body.pId == "") {
        whereDynamic1.p_id = { [Op.not]: null };
        whereDynamic2.p_id = { [Op.not]: null };
    } else {
        whereDynamic1.p_id = { [Op.substring]: req.body.pId  };
        whereDynamic2.p_id = { [Op.substring]: req.body.pId };
    }

    // console.log(whereDynamic)
    ExamTable.findAll({
        where: {
            ...whereDynamic1,
            // [Op.or]: [
            //     { 
            //         inv1: {
            //             [Op.substring] : req.body.inv
            //         }
            //     }, 
            //     { 
            //         inv2: {
            //             [Op.substring] : req.body.inv
            //         }
            //     }, 
            // ] 
        },
        order: [['id']]
    })
        .then((dataTable) => {
            var results = [];
            if (dataTable) {
                if (req.body.inv == null || req.body.inv == "") {
                    results.push(...dataTable);
                    res.status(200).json(results);
                } else {
                    ExamTable.findAll({
                        where: {
                            ...whereDynamic2,
                        },
                        order: [['id']]
                    })
                        .then((dataTable_sub) => {
                            results.push(...dataTable);
                            results.push(...dataTable_sub);
                            res.status(200).json(results);
                        })
                }
            } else {
                res.status(400).json({ result: 'There is no exam table  information in the system.' })
            }
        })
}

exports.insertExam = (req, res) => {
    const dataExam = {
        code_sub: req.body.codeSub,
        sub_name: req.body.subName,
        group_id: req.body.groupId,
        sec: req.body.sec,
        amount: req.body.amount,
        student_no: req.body.studentNo,
        student_no_end: req.body.studentNoEnd,
        room_name: req.body.roomName,
        room_build: req.body.roomBuild,
        teacher: req.body.teacher,
        inv1: req.body.inv1,
        inv2: req.body.inv2,
        date: req.body.date,
        p_id: req.body.pId,
        semester: req.body.semester,
        year: req.body.year,
        hte_id: req.body.hteId
    }

    ExamTable.create(dataExam).then(() => {
        res.status(200).json({ result: "Successfully !" })
    })
        .catch(err => {
            return res.status(500).json({ result: err.message })
        })

}


exports.getTableExamByCondition2 = (req, res) => {
    let whereDynamic1 = {
        hte_id: req.body.hteId
    }
    let whereDynamic2 = {
        hte_id: req.body.hteId
    }
    if (req.body.date == null || req.body.date == "") {
        whereDynamic1.date = { [Op.not]: null };
        whereDynamic2.date = { [Op.not]: null };
    } else {
        whereDynamic1.date = { [Op.substring]: req.body.date };
        whereDynamic2.date = { [Op.substring]: req.body.date };
    }
    if (req.body.perId == null || req.body.perId == "") {
        whereDynamic1.p_id = { [Op.not]: null };
        whereDynamic2.p_id = { [Op.not]: null };
    } else {
        whereDynamic1.p_id = { [Op.substring]: req.body.perId };
        whereDynamic2.p_id = { [Op.substring]: req.body.perId };
    }
    if (req.body.inv == null || req.body.inv == "") {
        whereDynamic1.inv1 = { [Op.not]: null };
        whereDynamic2.inv2 = { [Op.not]: null };
    } else {
        whereDynamic1.inv1 = { [Op.substring]: req.body.inv };
        whereDynamic2.inv2 = { [Op.substring]: req.body.inv };
    }
    // console.log(whereDynamic1)
    ExamTable.findAll({
        where: {
            ...whereDynamic1,
        },
        order: [['id']]
    })
        .then((data1) => {
            var result = [];
            if (data1) {
                ExamTable.findAll({
                    where: {
                        ...whereDynamic2,
                    },
                    order: [['id']]
                })
                    .then((data2) => {
                        result.push(...data1);
                        result.push(...data2);
                        res.status(200).json(result);
                    })
            } else {
                res.status(400).json({ result: 'There is no exam table  information in the system.' })
            }
        })
}

exports.editTableExamTable = (req, res) => {
    try {
        if (!!req.body.data) {
            let indexCheck = 0;
            for (const data of req.body.data) {
                const dataTableEx = {
                    inv1: data.inv1,
                    inv2: data.inv2,
                }
                ExamTable.update(dataTableEx, { where: { id: data.id } })
                    .then(() => {
                        indexCheck++;
                        if (indexCheck == req.body.data.length) {
                            return res.status(200).json({ status: 200, response: "Successfuly!" })
                        }
                    })
                    .catch((err) => {
                        return res.status(err.status).json({ message: err.message });
                    })
            }
        } else {
            return res.status(404).json({ message: "No data in array" })
        }
    } catch (e) {
        return res.status(500).json({ message: e.message })
    }
}

exports.getRoomDistinct = (req, res) => {
    try {
        ExamTable.findAll({ attributes: [[Sequelize.fn('DISTINCT', Sequelize.col('room_build')), 'room_build']] })
            .then((room) => {
                res.status(200).json({ status: 200, results: room })
            })

    } catch (e) {
        return res.status(500).json({ message: e.message })
    }
}

exports.getRoomTableExam = (req, res) => {
    try {
        ExamTable.findAll({
            where: {
                room_build: req.body.roomBuild,
                hte_id: req.body.hteId
            },
            order: [['id']]
        })
            .then((data) => {
                if (data.length > 0) {
                    res.status(200).json({ status: 200, result: data });
                } else {
                    res.status(404).json({ status: 404, result: "No Found" });
                }
            })
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}