const InvigilatorStatus = require("../Models/InvigilatorStatus");
const { verifyData } = require('../Middleware/index');

exports.getInvStatus = (req, res) => {
    InvigilatorStatus.findAll({
        attributes: [['IS_id', 'code'], ['IS_name', 'name']],
        order: [['IS_id']]
    })
        .then((invStatus) => {
            if (invStatus) {
                res.status(200).json(invStatus);
            } else {
                res.status(400).json({ result: 'There is no invigilator status information in the system.' })
            }
        })
        .catch((err) => {
            res.status(500).json({Error: err.message});
        })
}

exports.getInvStatusById = (req, res) => {
    InvigilatorStatus.findOne({
        where: { IS_id: req.params.isId },
        attributes: [['IS_id', 'code'], ['IS_name', 'name']]
    })
        .then((invStatus) => {
            if (invStatus) {
                res.status(200).json(invStatus);
            } else {
                res.status(400).json({ result: 'There is no invigilator status information in the system.' })
            }
        })
        .catch((err) => {
            res.status(500).json({Error: err.message});
        })
}

exports.createInvStatus = (req, res) => {
    if (req.body.isName) {
        InvigilatorStatus.findOne({ where: { IS_name: req.body.isName } })
            .then((invStatus) => {
                if (!invStatus) {
                    InvigilatorStatus.create({ IS_name: req.body.isName })
                        .then(() => {
                            res.status(200).json({ result: 'Successfully built !' });
                        })
                        .catch(err => {
                            res.status(500).json({Error: err.message});
                        });
                } else {
                    res.status(400).json({ result: 'Duplicate Invigilator Status information' });
                }
            })
    } else {
        res.status(400).json({ result: 'This is status name null.' });
    }
}

exports.editInvStatus = (req, res) => {
    InvigilatorStatus.findOne({
        where: {IS_id: req.params.isId },
    })
    .then((invStatus) => {
        if(invStatus){
            InvigilatorStatus.update({IS_name: verifyData.checkValue(req.body.isName, invStatus.IS_name)  }, {where:{IS_id: req.params.isId}})
            .then(() => {
                res.status(200).json({ result: 'Successfully update !' });
            })
            .catch(err => {
                res.status(500).json({Error: err.message});
            });
        }else {
            res.status(400).json({ result: 'There is no invigilator status information in the system.' })
        }
    })
}