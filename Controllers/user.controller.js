const User = require('../Models/User');
const { createCanvas } = require('canvas')
const canvas = createCanvas(600, 600)
const ctx = canvas.getContext('2d')

exports.updateProfile = (req, res) => {
    const userData = {
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        phone: req.body.phone,
    };

    User.findOne({
        where: {
            id: req.userId,
        }
    })
        .then((user) => {
            if (user) {
                User.update(userData, {
                    where: {
                        id: user.id,
                    }
                })
                    .then(() => {
                        res.status(200).json({ response: "Update user successfully!!" });
                    })
                    .catch((err) => {
                        res.status(500).json({ Error: err.message });
                    });

            } else {
                res.status(500).json({ message: "not find match id!" });
            }
        })
        .catch((err) => {
            res.status(500).json({ Error: "There is some error" });
        });
};

exports.getProfile = (req, res) => {
    User.findOne({
        where: {
            id: req.userId,
        }
    })
        .then((user) => {
            if (user) {
                ctx.font = '300px Impact'
                ctx.textAlign = "center";
                ctx.textBaseline = "middle";
                ctx.beginPath();
                ctx.rect(25, 20, 550, 550);
                ctx.fillStyle = "rgba(245, 40, 145, 0.5)";
                ctx.strokeStyle = 'rgba(245, 40, 145, 0.5)';
                ctx.lineWidth = 4;
                ctx.fill();
                ctx.strokeText(`${user.firstname[0]}${user.lastname[0]}`,30+(550/2),55+(550/2));
                ctx.fillText(`${user.firstname[0]}${user.lastname[0]}`, 30 + (550 / 2), 55 + (550 / 2));
                return res.status(200).json({
                    status: 200,
                    result: {
                        firstname: user.firstname,
                        lastname: user.lastname,
                        phone: user.phone,
                        img: canvas.toDataURL()
                    }
                })
            } else {
                return res.status(400).json({ result: "User not found" })
            }
        })
}