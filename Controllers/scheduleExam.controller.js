const Subject = require("../Models/Subject");
const ExamTable = require("../Models/ExamTable");
const { spawn } = require("child_process");
const path = require("path");
const fs = require("fs");
const { verifyData } = require("../Middleware/index");
const { config:{BASEURL} } = require('../Configs/server.config');

exports.getDataExam = (req, res) => {
  let date = new Date().toLocaleDateString("th-TH");
  Subject.findAll({
    where: {
      Sub_year: date.split("/")[2],
      Sub_semester: req.params.SubSemester,
    },
    attributes: [
      ["Sub_id", "subId"],
      ["Sub_code", "id"],
      ["Sub_name", "name"],
      ["Sub_sec", "sec"],
      ["Sub_group_id", "groupId"],
      ["Sub_amount", "amount"],
      ["Sub_teacher", "teacher"],
      ["Sub_datetime", "date"],
      ["Sub_year", "subYear"],
      ["Sub_semester", "subSemester"],
      ["Per_id", "perid"],
    ],
    order: [["Sub_id"]],
  })
    .then((data) => {
      if (data.length > 0) {
        res.status(200).json(data);
      } else {
        res.status(400).json({ results: "no information" });
      }
    })
    .catch((err) => {
      res.status(500).json({ err: err.message });
    });
};

exports.runAlgorithms = (req, res) => {
  const dataJson = path.join(__dirname, "../Other/json", "data.json");
  const pathRunAlgorithms = path.join(__dirname, "../Algorithms", "main.py");
  let token = req.headers["authorization"];
  console.log(req.body)
  var python = spawn('python3.9', [pathRunAlgorithms, token, req.body.startdate, req.body.enddate, req.body.date, BASEURL, req.body.hte_id]);
  python.stdout.on('data', function (data) {
    console.log('Pipe data from python script ...');
  });
  python.on('close', (code) => {
    console.log(`child process close all stdio with code ${code}`);
    fs.readFile(dataJson, 'utf8', (err, data) => {
      if (err) {
        console.log(`Error reading file from disk: ${err}`);
        res.status(500).json(err);
      } else {
        if (data.length > 0) {
          const datas = JSON.parse(data);
          var subjectlist = [];
          datas.forEach((subData) => {
            subData.list_sub.forEach((listSubData) => {
              subjectlist.push(
                {
                  "code_sub": listSubData.subCode,
                  "sub_name": listSubData.name,
                  "group_id": listSubData.groupId, //`${subData.list_sub[0].groupId} ${subData.list_sub[1].groupId??''}`,
                  "sec": listSubData.sec,
                  "amount": listSubData.amount,
                  "student_no": listSubData.startNo,
                  "student_no_end": listSubData.endNo,
                  "room_name": subData.name,
                  "room_build": subData.build,
                  "teacher": listSubData.teacher,
                  "inv1": `${subData.listInv[0].invFirstname??''} ${subData.listInv[0].invLastname??''}`,
                  "inv2": `${subData.listInv[1]?.invFirstname??''} ${subData.listInv[1]?.invLastname??''}`,
                  "date": req.body.date,
                  "p_id": verifyData.checkTime(listSubData.time),
                  "semester": req.body.semester,
                  "year": req.body.year,
                  "hte_id": parseInt(req.body.hte_id)
                }
              )
            })
          })
          ExamTable.bulkCreate(subjectlist, { returning: true })
            .then(() => {
              res.status(200).json({ status: 200 , response: 'ScheduleExam Successfully built !' })
              // res.status(200).json(subjectlist)
            })
            .catch(_e => {
              res.status(500).json({ error: _e })
            })
        }else {
          res.status(400).json({ status: 400 , response: 'ScheduleExam Successfully built !' })
        }
      }
    });
  });
};
