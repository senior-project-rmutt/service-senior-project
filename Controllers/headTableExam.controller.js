const HeadTableExam = require('../Models/HeadTableExam');
const ExamTable = require('../Models/ExamTable');

exports.getHeadTableExamList = (req, res) => {
    HeadTableExam.findAll({
        order: [['id']]
    })
        .then((data) => {
            if (data) {
                res.status(200).json(data);
            } else {
                res.status(400).json({ result: 'There is no header exam table  information in the system.' })
            }
        })
}

exports.getHeadTableExamListById = (req, res) => {
    HeadTableExam.findOne({
        where: { 
            id: req.params.id
        },
        attributes: [['name', 'name'], ['year', 'year'], ['semester', 'semester']],
        order: [['id']]
    })
        .then((data) => {
            if (data) {
                res.status(200).json(data);
            } else {
                res.status(400).json({ result: 'There is no header exam table  information in the system.' })
            }
        })
}

exports.getHeadTableExamListLatest = (req, res) => {
    HeadTableExam.findAll({
        order: [['id', 'DESC' ]]
    })
        .then((data) => {
            if (data) {
                res.status(200).json(data);
            } else {
                res.status(400).json({ result: 'There is no header exam table  information in the system.' })
            }
        })
}

exports.getHeadTableExamListDataLatest = (req, res) => {
    HeadTableExam.findAll({
        attributes: [['name', 'name'], ['year', 'year'], ['semester', 'semester']],
        order: [['id', 'DESC' ]]
    })
        .then((data) => {
            if (data) {
                res.status(200).json(data[0]);
            } else {
                res.status(400).json({ result: 'There is no header exam table  information in the system.' })
            }
        })
}

exports.createProjectTableExam = (req, res) => {
    const dataProjcetExam = {
        name: req.body.name,
        year: req.body.year,
        semester: req.body.semester,
    }

    HeadTableExam.create(dataProjcetExam)
        .then((data) => {
            if (data) {
                res.status(200).json(data);
            } else {
                res.status(400).json({ result: 'There is no header exam table  information in the system.' })
            }
        })
}

exports.deleteProjectTableExam = (req, res) => {
    ExamTable.destroy({
        where: {
            hte_id: req.params.id
        }
    })
    .then(() => {
        HeadTableExam.destroy({
            where: {
                id: req.params.id
            }
        })
            .then(() => {
                res.status(200).json({ result: 'Delete exam table Successfully !' });
            })
            .catch(err => {
                res.status(500).json({Error: err.message});
            })
    })
        
}