const Room = require('../Models/Room');
const RoomStatus = require('../Models/RoomStatus');

const formidable = require('formidable');
const fs = require('fs');
const path = require('path');
const XLSX = require('xlsx');

exports.getRoom = (req, res) => {
    Room.belongsTo(RoomStatus, { foreignKey: 'RS_id' });
    RoomStatus.hasMany(Room, { foreignKey: 'RS_id' });

    Room.findAll({
        include: [{
            model: RoomStatus,
            required: true,
            attributes: [['RS_id', 'id'], ['RS_name', 'name']]
        }],
        attributes: [['Room_id', 'roomId'], ['Room_name', 'roomName'], ['Room_amount', 'roomAmount'], ['Room_build', 'roomBuild']],
        order: [['Room_id']]
    })
        .then((rooms) => {
            if (rooms) {
                res.status(200).json(rooms);
            } else {
                res.status(400).json({ result: 'There is no room information in the system.' });
            }
        })
        .catch(err => {
            res.status(500).json({ Error: err.message });
        });
}

exports.getRoomToschedule = (req, res) => {
    Room.belongsTo(RoomStatus, { foreignKey: 'RS_id' });
    RoomStatus.hasMany(Room, { foreignKey: 'RS_id' });
    Room.findAll({
        include: [{
            model: RoomStatus,
            required: true,
            attributes: [['RS_id', 'id'], ['RS_name', 'name']]
        }],
        where: {
            RS_id: 1
        },
        attributes: [['Room_id', 'roomId'], ['Room_name', 'name'], ['Room_amount', 'amount'], ['Room_build', 'build']],
        order: [['Room_id']]
    })
        .then((rooms) => {
            if (rooms) {
                res.status(200).json({ room: rooms });
            } else {
                res.status(400).json({ result: 'There is no room information in the system.' });
            }
        })
        .catch(err => {
            res.status(500).json({ Error: err.message });
        });
}

exports.getRoomById = (req, res) => {
    Room.belongsTo(RoomStatus, { foreignKey: 'RS_id' });
    RoomStatus.hasMany(Room, { foreignKey: 'RS_id' });

    Room.findOne({
        where: {
            Room_id: req.params.roomId,
        },
        include: [{
            model: RoomStatus,
            required: true,
            attributes: [['RS_id', 'id'], ['RS_name', 'name']]
        }],
        attributes: [['Room_id', 'roomId'], ['Room_name', 'roomName'], ['Room_amount', 'roomAmount'], ['Room_build', 'roomBuild']],
    })
        .then((room) => {
            if (room) {
                res.status(200).json(room);
            } else {
                res.status(400).json({ result: 'There is no room information in the system.' });
            }
        })
        .catch(err => {
            res.status(500).json({ Error: err.message });
        });
}

exports.createRoom = (req, res) => {
    const roomData = {
        Room_name: req.body.roomName,
        Room_amount: req.body.roomAmount,
        Room_build: req.body.roomBuild,
        RS_id: req.body.rsId,
    };

    Room.findOne({
        where: {
            Room_name: req.body.roomName,
        }
    })
        .then((resRoom) => {
            if (!resRoom) {
                Room.create(roomData)
                    .then(() => {
                        res.status(200).json({ result: 'Successfully room built !' });
                    })
                    .catch(err => {
                        res.status(500).json({ Error: err.message });
                    });
            } else {
                res.status(500).json({ result: 'Duplicate room information' })
            }
        })
}

exports.editRoom = (req, res) => {
    const roomData = {
        Room_name: req.body.roomName,
        Room_amount: req.body.roomAmount,
        Room_build: req.body.roomBuild,
        RS_id: req.body.rsId,
    }
    Room.findOne({
        where: { Room_id: req.params.roomId }
    })
        .then(room => {
            if (room) {
                Room.update(roomData, { where: { Room_id: req.params.roomId } })
                    .then(() => {
                        res.status(200).json({ result: " Successfully Update !" });
                    })
                    .catch(err => {
                        res.status(500).json({ error: err })
                    });
            } else {
                res.status(400).json({ error: "Room number does not match this room" })
            }
        })
}

exports.fileCreateRoom = (req, res) => {
    var newPath;
    const form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        if (files.file.type === null) {
            return res.status(200).json({ msg: 'No file upload' });
        }

        var oldPath = files.file.path;
        var namefile = Date.now() + '_' + files.file.name;
        newPath = path.join(__dirname, '../uploads/rooms/')
            + namefile
        var rawData = fs.readFileSync(oldPath)

        fs.writeFile(newPath, rawData, function (e) {
            if (e) console.log(e)
            var workbook = XLSX.readFile(path.join(__dirname, '../uploads/rooms/' + namefile));
            var sheet_name_list = workbook.SheetNames;
            var xlData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);

            for (let data of xlData) {
                var status = data.status;
                if (data.status == "ปกติ") {
                    status = 1;
                } else if (data.status == "ปรับปรุง") {
                    status = 2;
                }
                const roomData = {
                    Room_name: data.name,
                    Room_amount: data.amount,
                    Room_build: data.build,
                    RS_id: status,
                }
                Room.findOne({
                    where: { Room_name: data.name }
                })
                    .then(room => {
                        if (room) {
                            Room.update(roomData, { where: { Room_name: data.name } })
                        } else {
                            Room.create(roomData)
                        }
                    })
                    .catch(_e => {
                        res.status(500).json({ Error: _e.message });
                    })
            }
            fs.unlinkSync(newPath);
            res.status(200).json({ status: 'Rooms Update and Successfully built !' })
        })
    })
}

exports.deleteRoomById = (req, res) => {
    Room.destroy({
        where: {
            Room_id: req.params.roomId
        }
    })
        .then(() => {
            res.status(200).json({ result: 'Delete room Successfully !' });
        })
        .catch(err => {
            res.status(500).json({ Error: err.message });
        })
}

exports.deleteRoomAll = (req, res) => {
    Room.findAll().then((dataRooms) => {
        if (dataRooms.length > 0){
            Room.destroy({
                where: {},
                truncate: true
            })
                .then(() => {
                    res.status(200).json({ status: 200, response: 'Delete room Successfully !' });
                })
                .catch(err => {
                    res.status(500).json({ Error: err.message });
                })
        }else {
            res.status(200).json({status: 200, response: 'Data Empty' })
        }
    })
}