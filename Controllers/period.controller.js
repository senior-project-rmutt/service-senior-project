const Period = require('../Models/Period');

exports.getLovPeriod = (req, res) => {
    Period.findAll({
        attributes: [['p_id', 'code'], ['p_name', 'name']],
        order: [['p_id']]
    })
    .then((data) => {
        if(!!data){
            return res.status(200).json(data);
        }else {
            return res.status(404).json({ressult: "not Found"});
        }
    })
    .catch((err) => {
        res.status(500).json({Error: err.message});
    })
}

exports.getLovPeriodTime = (req, res) => {
    Period.findAll({
        attributes: [['p_id', 'code'], ['p_startTime', 'startTime'], ['p_endTime', 'endTime']],
        order: [['p_id']]
    })
    .then((data) => {
        if(!!data){
            var result = [];
            data.forEach((dataDate) => {
                result.push({
                    code: dataDate.dataValues.code,
                    name: `${dataDate.dataValues.startTime} - ${dataDate.dataValues.endTime}`
                })
            })
            return res.status(200).json(result);
        }else {
            return res.status(404).json({ressult: "not Found"});
        }
    })
    .catch((err) => {
        res.status(500).json({Error: err.message});
    })
}