const controller = require('../Controllers/exportPDF.controller');
const { authJwt } = require('../Middleware');
const { config:{BASEURL} } = require('../Configs/server.config');

module.exports = function(app){
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.post(`${BASEURL}/route/export-pdf/create-exam-pdf` , controller.createExamPDF);
    app.post(`${BASEURL}/route/export-pdf/create-exam-pdf-v2` , controller.createExamPDFV2);
    app.delete(`${BASEURL}/route/export-pdf/delete-pdf/:filename` , controller.deletePDF);
    app.get(`${BASEURL}/file/export-pdf/get-exam-pdf/:id` , controller.getExamPDF);
    app.get(`${BASEURL}/image/logo/get-logo-rmutt` , controller.getImageLogo);
}