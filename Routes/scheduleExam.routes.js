const controller = require('../Controllers/scheduleExam.controller');
const { authJwt } = require('../Middleware');
const { config:{BASEURL} } = require('../Configs/server.config');
module.exports = function(app){
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get(`${BASEURL}/route/shedule-exam/getSubjects/:SubSemester`, [authJwt.verifyRefreshToken] , controller.getDataExam);
    app.post(`${BASEURL}/route/shedule-exam/run-script`, [authJwt.verifyRefreshToken] , controller.runAlgorithms);
}