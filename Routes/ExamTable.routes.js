const controller = require('../Controllers/examTable.controller');
const { authJwt } = require('../Middleware');
const { config:{BASEURL} } = require('../Configs/server.config');
module.exports = function(app){
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get(`${BASEURL}/route/exam-table/get-Exam-table/:hteId` , controller.getTableExamByHte);
    app.get(`${BASEURL}/route/exam-table/get-Exam-table` , controller.getTableExam);
    app.post(`${BASEURL}/route/exam-table/get-Exam-table-ByCondition` , controller.getTableExamByCondition);
    app.post(`${BASEURL}/route/exam-table/get-Exam-table-ByCondition2`,[authJwt.verifyToken] , controller.getTableExamByCondition2);
    app.post(`${BASEURL}/route/exam-date/create-Exam-table`, [authJwt.verifyToken] , controller.insertExam);
    app.post(`${BASEURL}/route/exam-date/edit-Exam-table`, [authJwt.verifyToken] , controller.editTableExamTable);
    app.get(`${BASEURL}/route/exam-table/get-exam-distinct`, [authJwt.verifyToken] , controller.getRoomDistinct);
    app.post(`${BASEURL}/route/exam-table/get-room-table-exam`, [authJwt.verifyToken] , controller.getRoomTableExam);
}