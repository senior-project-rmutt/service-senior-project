const controller = require('../Controllers/headTableExam.controller');
const { authJwt } = require('../Middleware');
const { config:{BASEURL} } = require('../Configs/server.config');

module.exports = function(app){
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get(`${BASEURL}/route/project-table/get-head-table-exam`, [authJwt.verifyToken] , controller.getHeadTableExamList);
    app.get(`${BASEURL}/route/project-table/get-head-table-exam/:id` , controller.getHeadTableExamListById);
    app.get(`${BASEURL}/route/project-table/get-head-table-exam-latest`, [authJwt.verifyToken] , controller.getHeadTableExamListLatest);
    app.post(`${BASEURL}/route/project-table/create-project-table`, [authJwt.verifyToken] , controller.createProjectTableExam);
    app.delete(`${BASEURL}/route/project-table/deleteProject-table-exam/:id`, [authJwt.verifyToken] , controller.deleteProjectTableExam);
    app.get(`${BASEURL}/route/project-table/get-head-table-exam-data-latest`, controller.getHeadTableExamListDataLatest);
}