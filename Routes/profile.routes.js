const controller = require('../Controllers/user.controller');
const { authJwt } = require('../Middleware');
const { config:{BASEURL} } = require('../Configs/server.config');

module.exports = function(app){
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get(`${BASEURL}/route/profile/getuser`, [authJwt.verifyToken] , controller.getProfile)

    app.put(`${BASEURL}/route/profile/editProfile`, [authJwt.verifyToken] , controller.updateProfile);
}