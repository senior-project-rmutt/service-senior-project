const controller = require('../../Controllers/invigilatorStatus.controller');
const { authJwt } = require('../../Middleware');
const { config:{BASEURL} } = require('../../Configs/server.config');

module.exports = function(app){
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get(`${BASEURL}/route/lov/inv-status/getInvStatus`, [authJwt.verifyToken] , controller.getInvStatus);
    app.get(`${BASEURL}/route/lov/inv-status/getInvStatus/:isId`, [authJwt.verifyToken] , controller.getInvStatusById);
    app.post(`${BASEURL}/route/lov/inv-status/createIsStatus`, [authJwt.verifyToken] , controller.createInvStatus);
    app.put(`${BASEURL}/route/lov/inv-status/edtiIsStatus/:isId`, [authJwt.verifyToken] , controller.editInvStatus);
    
}