const controller = require('../../Controllers/period.controller');
const { authJwt } = require('../../Middleware');
const { config:{BASEURL} } = require('../../Configs/server.config');
module.exports = function(app){
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get(`${BASEURL}/route/lov/period/getLovPeriod`, [authJwt.verifyToken] , controller.getLovPeriod);
    app.get(`${BASEURL}/route/lov/period/getLovPeriodTime` , controller.getLovPeriodTime);
}