const controller = require('../../Controllers/roomStatus.controller');
const { authJwt } = require('../../Middleware');
const { config:{BASEURL} } = require('../../Configs/server.config');
module.exports = function(app){
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get(`${BASEURL}/route/lov/room-status/getRoomStatus`, [authJwt.verifyToken] , controller.getRoomStatusAll);
    app.get(`${BASEURL}/route/lov/room-status/getRoomStatus/:rsId`, [authJwt.verifyToken] , controller.getRoomStatusById);
    app.post(`${BASEURL}/route/lov/room-status/createRoomStatus`, [authJwt.verifyToken] , controller.createRoomStatus);
    app.delete(`${BASEURL}/route/lov/room-status/deleteRoomStatus/:rsId`, [authJwt.verifyToken] , controller.deleteRoomStatusById);
    app.delete(`${BASEURL}/route/lov/room-status/deleteRoomStatusAll`, [authJwt.verifyToken] , controller.deleteRoomStatusAll);
    app.put(`${BASEURL}/route/lov/room-status/editRoomStatus/:rsId`, [authJwt.verifyToken] , controller.editRoomStatus);
}