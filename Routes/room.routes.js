const controller = require('../Controllers/room.controler');
const { authJwt } = require('../Middleware');
const { config:{BASEURL} } = require('../Configs/server.config');
module.exports = function(app){
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.post(`${BASEURL}/route/room/createRoom`, [authJwt.verifyRefreshToken] , controller.createRoom)
    app.get(`${BASEURL}/route/room/getRooms`, [authJwt.verifyRefreshToken] , controller.getRoom)
    app.get(`${BASEURL}/route/room/getRoom/:roomId`, [authJwt.verifyToken] , controller.getRoomById);
    app.post(`${BASEURL}/route/room/fileCreate`, [authJwt.verifyRefreshToken] , controller.fileCreateRoom);
    app.put(`${BASEURL}/route/room/editRoom/:roomId`, [authJwt.verifyRefreshToken] , controller.editRoom);
    app.delete(`${BASEURL}/route/lov/room/deleteRoom/:roomId`, [authJwt.verifyRefreshToken] , controller.deleteRoomById);
    app.delete(`${BASEURL}/route/room/delete-all`, [authJwt.verifyRefreshToken] , controller.deleteRoomAll);
    app.get(`${BASEURL}/route/room/getRoomToschedule`, [authJwt.verifyRefreshToken] , controller.getRoomToschedule)
}