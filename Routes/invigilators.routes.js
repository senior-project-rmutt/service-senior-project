const controller = require('../Controllers/invigilators.controller');
const { authJwt } = require('../Middleware');
const { config:{BASEURL} } = require('../Configs/server.config');
module.exports = function(app){
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get(`${BASEURL}/route/inv/getInv`, [authJwt.verifyToken] , controller.getInv);
    app.get(`${BASEURL}/route/inv/getInvToschedule`, [authJwt.verifyToken] , controller.getInvToschedule);
    app.get(`${BASEURL}/route/inv/getInv/:invId`, [authJwt.verifyToken] , controller.getInvById);
    app.post(`${BASEURL}/route/inv/fileCreate`, [authJwt.verifyToken] , controller.fileCreateInvigilator);
    app.put(`${BASEURL}/route/inv/updateInv/:invId`, [authJwt.verifyToken] , controller.editInv);
    app.post(`${BASEURL}/route/inv/createInv`, [authJwt.verifyToken] , controller.createInv);
    app.delete(`${BASEURL}/route/inv/delete-all`, [authJwt.verifyToken], controller.deleteInvigilators);
}