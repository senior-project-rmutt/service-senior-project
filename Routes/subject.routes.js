const controller = require('../Controllers/subject.controller');
const { authJwt } = require('../Middleware');
const { config:{BASEURL} } = require('../Configs/server.config');
module.exports = function(app){
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.post(`${BASEURL}/route/subject/createSubject`, [authJwt.verifyRefreshToken] , controller.createSubject)
    app.put(`${BASEURL}/route/subject/editSubject/:subId`, [authJwt.verifyRefreshToken] , controller.editSubject);
    app.get(`${BASEURL}/route/subject/getSubjects`, [authJwt.verifyRefreshToken] , controller.getSubjects)
    app.get(`${BASEURL}/route/subject/getSubject/:subjectId`, [authJwt.verifyRefreshToken] , controller.getSubjectsById);
    app.post(`${BASEURL}/route/subject/fileCreate`, [authJwt.verifyRefreshToken] , controller.fileCreateSubject);
    app.delete(`${BASEURL}/route/subject/deleteSubject/:subId` , [authJwt.verifyRefreshToken] , controller.deleteSubjectById);
    app.delete(`${BASEURL}/route/subject/delete-all` , [authJwt.verifyRefreshToken] , controller.deleteSubjectAll);
    app.post(`${BASEURL}/route/subject/getSubjectsToSchedule`, [authJwt.verifyRefreshToken] , controller.getSubjectsToSchedule)
}