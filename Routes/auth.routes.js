const controller = require('../Controllers/auth.controller');
const { config:{BASEURL} } = require('../Configs/server.config');
module.exports = function(app) {
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    // app.post('/api/auth/login',controller.login);

    app.post(`${BASEURL}/auth/register`, controller.register);
}