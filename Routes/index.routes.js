module.exports = function(app){
    require('./auth.routes')(app);
    require('./profile.routes')(app);
    require('./invigilators.routes')(app);
    require('./room.routes')(app);
    require('./subject.routes')(app);
    require('./ExamDate.routes')(app);
    require('./ExamTable.routes')(app);
    require('./scheduleExam.routes')(app);
    require('./ExportPDF.routes')(app);
    require('./projectTableExam.routes')(app);

    // Lov
    require('./lov/roomStatus.routes')(app);
    require('./lov/invigilatorStatus.routes')(app);
    require('./lov/position.routes')(app);
    require('./lov/period.routes')(app);
    require('./lov/examDateLov.routes')(app);
    require('./lov/invigilatorLov.routes')(app);
}