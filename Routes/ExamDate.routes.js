const controller = require('../Controllers/examDate.controller');
const { authJwt } = require('../Middleware');
const { config:{BASEURL} } = require('../Configs/server.config');

module.exports = function(app){
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get(`${BASEURL}/route/exam-date/get-Exam-Date`, [authJwt.verifyToken] , controller.getExamDateAll);
    app.get(`${BASEURL}/route/exam-date/get-Exam-DateToSchedule/:hteId`, [authJwt.verifyToken] , controller.getExamDateAllToSchedule);
    app.post(`${BASEURL}/route/exam-date/create-ExamDate`, [authJwt.verifyToken] , controller.createExamDate);
    app.post(`${BASEURL}/route/exam-date/bulk-create-ExamDate`, [authJwt.verifyToken] , controller.bulkCreateExamDate);
    app.put(`${BASEURL}/route/exam-date/update-ExamDate/:edId`, [authJwt.verifyToken] , controller.editExamDate);
    app.get(`${BASEURL}/route/exam-date/get-Exam-Date-by-Hte/:hteId`, [authJwt.verifyToken] , controller.getExamDateAllByHte);
    app.get(`${BASEURL}/route/exam-date/get-ExamDateById/:edId`, [authJwt.verifyToken] , controller.getExamDateById);
    app.delete(`${BASEURL}/route/exam-date/deleteExamDate/:edId` , [authJwt.verifyToken] , controller.deleteById);
    app.delete(`${BASEURL}/route/exam-date/deleteExamDateByHte/:hteId` , [authJwt.verifyToken] , controller.deleteByHteId);
    app.delete(`${BASEURL}/route/exam-date/deleteExamDateAll` , [authJwt.verifyToken] , controller.deleteAll);
    app.get(`${BASEURL}/route/exam-date/getPubExamDate/:hteId` , controller.getPubExamDate);
}