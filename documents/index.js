module.exports = ({ user_id, Fullname, user_gender, branch_name, faculty_name, type_id, score, user_phone }) => {
    const today = new Date();
    function gender() {
       switch (user_gender) {
          case "M":
             return '<hr style="position: absolute; top: 21.4%; left: 26.5%; transform: translate(-50%, -50%); border-top: 1px solid;" width="40px">';
          case "F":
             return '<hr style="position: absolute; top: 21.4%; left: 22%; transform: translate(-50%, -50%); border-top: 1px solid;" width="38px">'
       }
    }
 
    function typesexam() {
       switch (type_id) {
          case 1:
             return '<p style="position: absolute; top: 34%;   left: 12.3%; font-size: 7px; transform: translate(-50%, -50%);"><i class="fa fa-check fa-lg" aria-hidden="true"></i></p><p style="position: absolute; top: 34%; left: 80%; font-size: 7px; transform: translate(-50%, -50%);">'+score+'</p>';
          case 2:
             return '<p style="position: absolute; top: 36%;   left: 12.3%; font-size: 7px; transform: translate(-50%, -50%);"><i class="fa fa-check fa-lg" aria-hidden="true"></i></p><p style="position: absolute; top: 35.8%; left: 80%; font-size: 7px; transform: translate(-50%, -50%);">'+score+'</p>';
          case 3:
             return '<p style="position: absolute; top: 37.9%; left: 12.3%; font-size: 7px; transform: translate(-50%, -50%);"><i class="fa fa-check fa-lg"  aria-hidden="true"></i></p><p style="position: absolute; top: 37.6%; left: 80%; font-size: 7px; transform: translate(-50%, -50%);">'+score+'</p>';
          case 4:
             return '<p style="position: absolute; top: 39.7%; left: 12.3%; font-size: 7px; transform: translate(-50%, -50%);"><i class="fa fa-check fa-lg"  aria-hidden="true"></i></p><p style="position: absolute; top: 39.5%; left: 80%; font-size: 7px; transform: translate(-50%, -50%);">'+score+'</p>';
          case 5:
             return '<p style="position: absolute; top: 41.5%; left: 12.3%; font-size: 7px; transform: translate(-50%, -50%);"><i class="fa fa-check fa-lg"  aria-hidden="true"></i></p><p style="position: absolute; top: 41.3%; left: 80%; font-size: 7px; transform: translate(-50%, -50%);">'+score+'</p>';
          case 6:
             return '<p style="position: absolute; top: 43.5%; left: 12.3%; font-size: 7px; transform: translate(-50%, -50%);"><i class="fa fa-check fa-lg"  aria-hidden="true"></i></p><p style="position: absolute; top: 43.2%; left: 80%; font-size: 7px; transform: translate(-50%, -50%);">'+score+'</p>';
          case 7:
             return '<p style="position: absolute; top: 45.3%; left: 12.3%; font-size: 7px; transform: translate(-50%, -50%);"><i class="fa fa-check fa-lg"  aria-hidden="true"></i></p><p style="position: absolute; top: 45%; left: 80%; font-size: 7px; transform: translate(-50%, -50%);">'+score+'</p>';
          case 8:
             return '<p style="position: absolute; top: 47.3%;   left: 12.3%; font-size: 7px; transform: translate(-50%, -50%);"><i class="fa fa-check fa-lg"  aria-hidden="true"></i></p><p style="position: absolute; top: 47%; left: 80%; font-size: 7px; transform: translate(-50%, -50%);">'+score+'</p>';
          case 9:
             return '<p style="position: absolute; top: 49%; left: 12.3%; font-size: 7px; transform: translate(-50%, -50%);"><i class="fa fa-check fa-lg"  aria-hidden="true"></i></p><p style="position: absolute; top: 49%; left: 80%; font-size: 7px; transform: translate(-50%, -50%);">'+score+'</p>';
          case 10:
             return '<p style="position: absolute; top: 50.9%; left: 12.3%; font-size: 7px; transform: translate(-50%, -50%);"><i class="fa fa-check fa-lg"  aria-hidden="true"></i></p><p style="position: absolute; top: 50.5%; left: 80%; font-size: 7px; transform: translate(-50%, -50%);">'+score+'</p>';
          case 11:
             return '<p style="position: absolute; top: 52.7%; left: 12.3%; font-size: 7px; transform: translate(-50%, -50%);"><i class="fa fa-check fa-lg"  aria-hidden="true"></i></p><p style="position: absolute; top: 52.5%; left: 80%; font-size: 7px; transform: translate(-50%, -50%);">'+score+'</p>';
          case 12:
             return '<p style="position: absolute; top: 54.5%; left: 12.3%; font-size: 7px; transform: translate(-50%, -50%);"><i class="fa fa-check fa-lg"  aria-hidden="true"></i></p><p style="position: absolute; top: 54.3%; left: 80%; font-size: 7px; transform: translate(-50%, -50%);">'+score+'</p>';
       }
    }
    return `
     <!doctype html>
     <html>
        <head>
           <meta charset="utf-8">
           <title>PDF Result Template</title>
           <style>
           </style>
           <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        </head>
        <body>
          <div class="container" style="position: relative;display: inline-block; ">
             <img src="http://localhost:5000/Eng_form.jpg"; style="width:100%; height:100%;">
             <p style="position: absolute; top: 17.5%; right: 90px; font-size: 7px;">${`${today.getDate()}/${today.getMonth() + 1}/${today.getFullYear()}`}</p>
             <p style="position: absolute; top: 20.8%; left: 40%; font-size: 7px; transform: translate(-50%, -50%);">${Fullname}</p>
             <p style="position: absolute; top: 20.8%; left: 75%; font-size: 7px; transform: translate(-50%, -50%);">${user_id}</p>
             ${gender()}
             <p style="position: absolute; top: 24.6%; left: 25%; font-size: 7px; transform: translate(-50%, -50%);">${branch_name}</p>
             <p style="position: absolute; top: 24.6%; left: 47%; font-size: 7px; transform: translate(-50%, -50%);">${faculty_name}</p>
             <p style="position: absolute; top: 24.6%; left: 80%; font-size: 7px; transform: translate(-50%, -50%);">${user_phone}</p>
             ${typesexam()}
             <p style="position: absolute; top: 66%; left: 63%; font-size: 7px; transform: translate(-50%, -50%);">${Fullname}</p>
          </div>
        </body>
     </html>
     `;
 };