const Sequelize = require('sequelize');
const DB = require('../Database/DB');

module.exports = DB.sequelize.define(
    'RegistrationOfficer',
    {
        Officer_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        Officer_username: {
            type: Sequelize.STRING,
            unique: true,
        },
        Officer_password: {
            type: Sequelize.STRING
        },
        Officer_firstname: {
            type: Sequelize.STRING
        },
        Officer_lastname: {
            type: Sequelize.STRING
        },
        Officer_created_at: {
            type: Sequelize.DATE
        },
        Officer_updated_at: {
            type: Sequelize.DATE,
        },
    },
    {
        timestamps: false,
        freezeTableName: true,
        tableName: 'RegistrationOfficer'
    }
)
