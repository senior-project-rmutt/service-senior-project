const Sequelize = require('sequelize');
const DB = require('../Database/DB');

module.exports = DB.sequelize.define(
    'examdate',
    {
        Ed_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        Ed_date: {
            type: Sequelize.DATEONLY,
        },
        // Ed_timeStart: {
        //     type: Sequelize.TIME
        // },
        // Ed_timeEnd: {
        //     type: Sequelize.TIME
        // },
        hte_id: {
            type: Sequelize.INTEGER
        }
    },
    {
        timestamps: false,
        freezeTableName: true,
        tableName: 'examdate'
    }
)
