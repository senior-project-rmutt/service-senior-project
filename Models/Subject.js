const Sequelize = require('sequelize');
const DB = require('../Database/DB');

module.exports = DB.sequelize.define(
    'subjects',
    {
        Sub_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        Sub_code: {
            type: Sequelize.STRING,
        },
        Sub_name: {
            type: Sequelize.STRING,
        },
        Sub_sec: {
            type: Sequelize.INTEGER,
        },
        Sub_group_id: {
            type: Sequelize.STRING,
        },
        Sub_amount: {
            type: Sequelize.INTEGER,
        },
        Sub_teacher: {
            type: Sequelize.STRING,
        },
        Sub_datetime: {
            type: Sequelize.DATE,
        },
        Sub_year: {
            type: Sequelize.INTEGER,
        },
        Sub_semester: {
            type: Sequelize.STRING,
        },
        Per_id: {
            type: Sequelize.INTEGER,
        },
    },
    {
        timestamps: false,
        freezeTableName: true,
        tableName: 'subjects'
    }
)
