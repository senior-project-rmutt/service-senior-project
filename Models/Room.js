const Sequelize = require('sequelize');
const DB = require('../Database/DB');

module.exports = DB.sequelize.define(
    'rooms',
    {
        Room_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        Room_name: {
            type: Sequelize.STRING,
            unique: true,
        },
        Room_amount: {
            type: Sequelize.INTEGER
        },
        Room_build: {
            type: Sequelize.STRING
        },
        RS_id: {
            type: Sequelize.INTEGER,
        },
    },
    {
        timestamps: false,
        freezeTableName: true,
        tableName: 'rooms'
    }
)
