const Sequelize = require('sequelize');
const DB = require('../Database/DB');

module.exports = DB.sequelize.define(
    'head_table_exam',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING,
        },
        year: {
            type: Sequelize.INTEGER,
        },
        semester: {
            type: Sequelize.INTEGER,
        },
    },
    {
        timestamps: false,
        freezeTableName: true,
        tableName: 'head_table_exam'
    }
)
