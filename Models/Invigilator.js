const Sequelize = require('sequelize');
const DB = require('../Database/DB');

module.exports = DB.sequelize.define(
    'invigilator',
    {
        Inv_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        Inv_prefix: {
            type: Sequelize.STRING,
        },
        Inv_firstname: {
            type: Sequelize.STRING,
        },
        Inv_lastname: {
            type: Sequelize.STRING,
        },
        Inv_phone: {
            type: Sequelize.STRING
        },
        Inv_lineid: {
            type: Sequelize.STRING
        },
        IS_id: {
            type: Sequelize.INTEGER,
        },
        Pt_id: {
            type: Sequelize.INTEGER,
        },
    },
    {
        timestamps: false,
        freezeTableName: true,
        tableName: 'invigilator'
    }
)
