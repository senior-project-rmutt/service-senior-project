const Sequelize = require('sequelize');
const DB = require('../Database/DB');

module.exports = DB.sequelize.define(
    'table-exam',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        code_sub: {
            type: Sequelize.STRING,
        },
        sub_name: {
            type: Sequelize.STRING,
        },
        group_id: {
            type: Sequelize.STRING,
        },
        sec: {
            type: Sequelize.INTEGER,
        },
        amount: {
            type: Sequelize.INTEGER,
        },
        student_no: {
            type: Sequelize.INTEGER,
        },
        student_no_end: {
            type: Sequelize.INTEGER,
        },
        room_name: {
            type: Sequelize.STRING,
        },
        room_build: {
            type: Sequelize.STRING,
        },
        teacher: {
            type: Sequelize.STRING,
        },
        inv1: {
            type: Sequelize.STRING,
        },
        inv2: {
            type: Sequelize.STRING,
        },
        date: {
            type: Sequelize.DATE,
        },
        p_id: {
            type: Sequelize.INTEGER,
        },
        semester : {
            type: Sequelize.INTEGER,
        },
        year : {
            type: Sequelize.INTEGER,
        },
        hte_id : {
            type: Sequelize.INTEGER,
        },
    },
    {
        timestamps: false,
        freezeTableName: true,
        tableName: 'table-exam'
    }
)
