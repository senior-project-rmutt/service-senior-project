const Sequelize = require('sequelize');
const DB = require('../Database/DB');

module.exports = DB.sequelize.define(
    'period',
    {
        p_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        p_name: {
            type: Sequelize.STRING,
        },
        p_startTime: {
            type: Sequelize.STRING,
        },
        p_endTime: {
            type: Sequelize.STRING,
        },
    },
    {
        timestamps: false,
        freezeTableName: true,
        tableName: 'period'
    }
)
