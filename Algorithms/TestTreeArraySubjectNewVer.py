# -*- coding: utf-8 -*-
import json
import datetime
from pprint import pprint
import random
import math
import functools
import asyncio
import time


make_period = {
    "time": [
        {
            "period": "เช้า",
            "time": "09:00:00",
        },
        {
            "period": "บ่าย",
            "time": "13:00:00",
        },
        {
            "period": "ค่ำ",
            "time": "17:00:00",
        }

    ]
}


class Node:
    def __init__(self, period):
        self.invs = None
        self.room_earlymorning = []
        self.room_afternoon = []
        self.room_evening = []
        self.subject = None
        self.date = None
        self.period = period
        self.sub_earlymorning = []
        self.sub_afternoon = []
        self.sub_evening = []
        self.result_sub_earlymorning = []
        self.result_sub_afternoon = []
        self.result_sub_evening = []

    async def initdata(self, date, inv):
        self.date = date
        self.invs = random.sample(inv, len(inv))

    def condition_subject(self, subject):
        self.subject = subject
        for data in self.subject[0]['subject']:
            data["part"] = 1
            data["amountClass"] = data["amount"]
            data["startNo"] = 1
            data["endNo"] = data["amount"]
        for i in range(len(self.subject[0]['subject'])):
            # pprint(i)
            if self.subject[0]["subject"][i]["time"] == '09:00:00':
                self.sub_earlymorning.append(self.subject[0]["subject"][i])

            elif self.subject[0]["subject"][i]["time"] == '13:00:00':
                self.sub_afternoon.append(self.subject[0]["subject"][i])

            elif self.subject[0]["subject"][i]["time"] == '17:00:00':
                self.sub_evening.append(self.subject[0]["subject"][i])

    async def push_subject_of_room(self, rooms, subject):
        self.sub_earlymorning = subject
        i = 0
        self.room_earlymorning = rooms
        for room in range(len(self.room_earlymorning)):
            self.room_earlymorning[room]["list_sub"] = []

        i_room = 0
        for i_sub in range(len(self.sub_earlymorning)):
            while i < len(self.sub_earlymorning[i_sub]):
                if i_room < len(self.room_earlymorning):
                    if len(self.room_earlymorning[i_room]["list_sub"]) < 2:
                        if len(self.room_earlymorning[i_room]["list_sub"]) == 1:
                            if self.room_earlymorning[i_room]["list_sub"][0]["subCode"] == self.sub_earlymorning[i_sub][i]["subCode"]:
                                if i_room == len(self.room_earlymorning):
                                    i_room = 0
                                i_room += 1
                            else:
                                if root.check_subject_and_room(self.sub_earlymorning[i_sub][i], self.room_earlymorning[i_room]):
                                    self.room_earlymorning[i_room]["amount"] = self.room_earlymorning[i_room]["amount"] - \
                                        self.sub_earlymorning[i_sub][i]["amount"]
                                    self.room_earlymorning[i_room]["list_sub"].append(
                                        self.sub_earlymorning[i_sub][i])
                                    self.room_earlymorning[i_room]["listInv"] = [
                                    ]
                                    i += 1
                                else:
                                    root.number_of_people(
                                        self.sub_earlymorning[i_sub][i], self.room_earlymorning[i_room], i_sub)
                        else:
                            if root.check_subject_and_room(self.sub_earlymorning[i_sub][i], self.room_earlymorning[i_room]):
                                self.room_earlymorning[i_room]["amount"] = self.room_earlymorning[i_room]["amount"] - \
                                    self.sub_earlymorning[i_sub][i]["amount"]
                                self.room_earlymorning[i_room]["list_sub"].append(
                                    self.sub_earlymorning[i_sub][i])
                                self.room_earlymorning[i_room]["listInv"] = []
                                i += 1
                            else:
                                root.number_of_people(
                                    self.sub_earlymorning[i_sub][i], self.room_earlymorning[i_room], i_sub)

                    else:
                        if i_room == len(self.room_earlymorning):
                            i_room = 0
                        i_room += 1
                else:
                    break
            i = 0
            i_room = 0
            self.room_earlymorning = rooms

        self.result_sub_earlymorning = self.room_earlymorning

        return self.room_earlymorning

    def random_room(self, rooms):
        self.room_earlymorning = random.sample(
            rooms["room"], len(rooms["room"]))
        return self.room_earlymorning

    def number_of_people(self, subject, room, i_sub):
        result_room_amount = room["amount"] - 3
        sub_start_no = 0
        sub_end_no = 0

        result_sub_amount = subject["amount"] - result_room_amount

        if subject["part"] == 1:
            sub_start_no = 1
            sub_end_no = result_room_amount
        else:
            sub_start_no = subject["startNo"]
            sub_end_no = subject["amountClass"]

        subject["amount"] = result_room_amount
        subject["startNo"] = sub_start_no
        subject["endNo"] = sub_end_no
        ###################################################
        if subject["time"] == '09:00:00' or subject["time"] == '13:00:00' or subject["time"] == '17:00:00':
            self.sub_earlymorning[i_sub].append(
                {
                    "subCode": subject["subCode"],
                    "name": subject["name"],
                    "groupId": subject["groupId"],
                    "sec": subject["sec"],
                    "amount": result_sub_amount,
                    "teacher": subject["teacher"],
                    "date": subject["date"],
                    "time": subject["time"],
                    "subYear": subject["subYear"],
                    "subSemester": subject["subSemester"],
                    "part": subject["part"]+1,
                    "amountClass": subject["amountClass"],
                    "startNo": sub_end_no + 1,
                    "endNo": subject["amountClass"],
                    "teacherFname": subject["teacherFname"],
                    "teacherLname": subject["teacherLname"],
                    "teacherTitle": subject["teacherTitle"]
                }
            )

    def check_subject_and_room(self, subject, room):
        if subject["amount"] < room["amount"] and len(room["list_sub"]) == 0:
            return True
        elif subject["amount"] <= room["amount"] and len(room["list_sub"]) == 1:
            return True
        else:
            return False

    def process_algorithm(subject, rooms, date, inv):
        data_rooms = root.random_room(rooms)
        asyncio.run(root.initdata(date, inv["result"]))
        return asyncio.run(root.push_subject_of_room(data_rooms, subject))

    async def push_invandtech_rooms(self, result_data):
        inv = []
        i = 0
        teacher = []
        for i in range(len(self.invs)):
            if self.invs[i]["position"]["id"] == 1:
                inv.append(self.invs[i])
            else:
                teacher.append(self.invs[i])

        for i in range(len(result_data)):
            if len(inv) > 0:
                inv_random = random.choice(inv)
            else:
                inv_random = None
            if len(teacher) > 0:
                teacher_random = random.choice(teacher)
            else:
                inv_random = None
            if len(result_data[i]["listInv"]) == 0:
                if len(teacher) > 0:
                    if len(result_data[i]["list_sub"]) == 2:
                        teacher_text1 = result_data[i]["list_sub"][0]["teacherFname"]
                        teacher_text2 = result_data[i]["list_sub"][1]["teacherFname"]
                        if teacher_text1 != teacher_random["invFirstname"] and teacher_text2 != teacher_random["invFirstname"]:
                            result_data[i]["listInv"].append(teacher_random)
                        else:
                            teacher_random = random.choice(teacher)
                            result_data[i]["listInv"].append(teacher_random)
                    if len(result_data[i]["list_sub"]) == 1:
                        teacher_text1 = result_data[i]["list_sub"][0]["teacherFname"]
                        if teacher_text1 != teacher_random["invFirstname"]:
                            result_data[i]["listInv"].append(teacher_random)
                        else:
                            teacher_random = random.choice(teacher)
                            result_data[i]["listInv"].append(teacher_random)
                    if teacher_random:
                        index = next((i
                                      for i, item in enumerate(teacher) if item["invId"] == teacher_random["invId"]), -1)
                        teacher.pop(index)
                        teacher = random.sample(teacher, len(teacher))
            if len(result_data[i]["listInv"]) == 1:
                if len(inv) > 0:
                    result_data[i]["listInv"].append(inv_random)
                    if inv_random:
                        index = next((i
                                      for i, item in enumerate(inv) if item["invId"] == inv_random["invId"]), -1)
                        inv.pop(index)
                        inv = random.sample(inv, len(inv))
                elif len(teacher) > 0:
                    teacher_random = random.choice(teacher)
                    if len(result_data[i]["list_sub"]) == 2:
                        teacher_text1 = result_data[i]["list_sub"][0]["teacherFname"]
                        teacher_text2 = result_data[i]["list_sub"][1]["teacherFname"]
                        if teacher_text1 != teacher_random["invFirstname"] and teacher_text2 != teacher_random["invFirstname"]:
                            result_data[i]["listInv"].append(teacher_random)
                        else:
                            teacher_random = random.choice(teacher)
                            result_data[i]["listInv"].append(teacher_random)
                    if len(result_data[i]["list_sub"]) == 1:
                        teacher_text1 = result_data[i]["list_sub"][0]["teacherFname"]
                        if teacher_text1 != teacher_random["invFirstname"]:
                            result_data[i]["listInv"].append(teacher_random)
                        else:
                            teacher_random = random.choice(teacher)
                            result_data[i]["listInv"].append(teacher_random)
                    if teacher_random:
                        index = next((i
                                      for i, item in enumerate(teacher) if item["invId"] == teacher_random["invId"]), -1)
                        teacher.pop(index)
                        teacher = random.sample(teacher, len(teacher))
        return result_data

    def random_inv_teacher(result_data):
        return asyncio.run(root.push_invandtech_rooms(result_data))


root = Node(make_period)
