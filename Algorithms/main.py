import json
from pprint import pprint
import TestTreeArraySubjectNewVer as TestTreeArraySubjectNewVer
import os
import sys
import requests

subjects = []
sub_earlymorning = []
sub_afternoon = []
sub_evening = []
result_sub_earlymorning = None
result_sub_afternoon = None
result_subjected = []
subjects_arry = []
result_data = []
result_finaly = []
root = "/var/www/subdomain/sci-exam/service-senior-project"
path = "Other/json"


def condition_subject(subject):
    sub_earlymorning.clear()
    subjects = subject
    for data in subjects['subject']:
        data["part"] = 1
        data["amountClass"] = data["amount"]
        data["startNo"] = 1
        data["endNo"] = data["amount"]

    for i in range(len(subjects['subject'])):
        if subjects["subject"][i]["time"] == '09:00:00':
            sub_earlymorning.append(subjects["subject"][i])

        elif subjects["subject"][i]["time"] == '13:00:00':
            sub_afternoon.append(subjects["subject"][i])

        elif subjects["subject"][i]["time"] == '17:00:00':
            sub_evening.append(subjects["subject"][i])

    if len(sub_earlymorning) > 0:
        subjects_arry.append(sub_earlymorning)
    if len(sub_afternoon) > 0:
        subjects_arry.append(sub_afternoon)
    if len(sub_evening) > 0:
        subjects_arry.append(sub_evening)
    return True


def push_array(data):
    for data_i in data:
        if len(data_i["list_sub"]) > 0:
            result_subjected.append(data_i)


def start_process(subject, rooms, date, teachersandinv):
    for i in range(1):
            condition_subject(subject[i])
            result_sub_earlymorning = TestTreeArraySubjectNewVer.Node.process_algorithm(
                subjects_arry, rooms, date, teachersandinv)
            if len(result_sub_earlymorning) > 0:
                push_array(result_sub_earlymorning)
                subjects_arry.clear()
            result_data = TestTreeArraySubjectNewVer.Node.random_inv_teacher(
                result_subjected)
            result_finaly.append(result_data)

    with open(os.path.join(root,path, "data.json"), 'w') as f:
        json.dump(result_finaly[0], f)


token = sys.argv[1]
myobj = {'startdate': sys.argv[2], 'enddate':sys.argv[3], 'date':sys.argv[4]}

Inv = requests.get("http://localhost:8000{}/route/inv/getInvToschedule".format(sys.argv[5]), headers={
                           'Content-Type': 'application/json', 'Authorization': token}).json()
Date = requests.get("http://localhost:8000{}/route/exam-date/get-Exam-DateToSchedule/{}".format(sys.argv[5],sys.argv[6]), headers={
                            'Content-Type': 'application/json', 'Authorization': token}).json()
Room = requests.get("http://localhost:8000{}/route/room/getRoomToschedule".format(sys.argv[5]), headers={
                            'Content-Type': 'application/json', 'Authorization': token}).json()
Subject = requests.post("http://localhost:8000{}/route/subject/getSubjectsToSchedule".format(sys.argv[5]) , data = json.dumps(myobj), headers={
                               'Content-Type': 'application/json', 'Authorization': token}).json()

pprint(start_process(Subject,Room,Date,Inv))
