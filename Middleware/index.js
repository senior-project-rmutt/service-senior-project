const authJwt = require('./authJwt');
const verifyData = require('./verifyData');

module.exports = {
    authJwt,
    verifyData,
};