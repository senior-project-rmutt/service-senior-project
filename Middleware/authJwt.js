const jwt = require('jsonwebtoken');
const config = require('../Configs/auth.config');

var verifyToken = (req, res, next) => {
    let token = req.headers["authorization"];
    // console.log(token)
    if (!token) {
        return res.status(401).send({ message: "No token provided!" });
    }
    const bearer = token.split(' ')[1];
    jwt.verify(bearer, config.secret, (err, decoded) => {
        if(err){
            return res.status(401).send({ message: "Unauthorized"});
        }
        req.userId = decoded.sub.id;
        req.role = decoded.sub.role;
        next();
    });

};

var verifyRefreshToken = (req, res, next) => {
    let refreshToken = req.headers["authorization"];
    if(!refreshToken){
        return res.status(401).send({ message: "No token provided!" });
    }
    const bearerRefresh = refreshToken.split(' ')[1];
    jwt.verify(bearerRefresh, config.secret, (err, decoded) => {
        if(err){
            return res.status(401).send({ message: "Unauthorized"});
        }
        req.userId = decoded.sub.id;
        req.role = decoded.sub.role;
        next();
    });
}

const authJwt = {
    verifyToken: verifyToken,
    verifyRefreshToken: verifyRefreshToken,
};

module.exports = authJwt;