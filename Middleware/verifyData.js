exports.checkValue = function (value, oldValue) {
    if (value === '' || value === undefined || value === null) {
        return oldValue;
    } else {
        return value;
    }
}

exports.checkTime = function (value) {
    if (value == "09:00:00") {
        return 1;
    } else if (value == "13:00:00") {
        return 2;
    } else if (value == "17:00:00") {
        return 3;
    }
}